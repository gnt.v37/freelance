<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="Assets/Css/library.css">
<link rel="stylesheet" href="Assets/Css/default.css">
<link rel="stylesheet" href="User/Css/login.css">
<title>Login</title>

</head>
<body>
	<div>
		<div class="__mar-t-128">
			<div class="__w462">
				<div class="__imar">
					<center>
						<img class="__iuser" alt="" src="User/Img/user_blue.png">
					</center>
				</div>

				<center class="__mar-b-16">
					<h4 class="__title">Đăng Nhập</h4>
				</center>
				<html:form action="/LoginProcess" method="POST">
					<div class="__mar-b-16">
						<input type="email" class="__txa" name="email"
							placeholder="example@gmail.com" autofocus="true" />
					</div>
					<div class="__mar-b-16">
						<input type="password" class="__txa" name="password"
							placeholder="password" />
					</div>
					<div class="__crimson __pad-b-12">
						<bean:define id="error" name="fLogin" property="accountError"></bean:define>
						${ error }
					</div>
					<div>
						<html:submit styleClass="__inp">Đăng Nhập</html:submit>
					</div>
				</html:form>
				<div class="__mar-t-16">
					<div class="__center">
						<div class="__il-b">Bạn chưa có tài khoản?</div>
						<div class="__il-b">
							<a href="Register.do" class="__color">Đăng Ký</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>