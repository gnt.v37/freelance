<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<bean:define id="user" name="fUserProfile" property="user"></bean:define>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="Assets/Css/library.css">
<link rel="stylesheet" href="Assets/Css/font-awesome.min.css">
<link rel="stylesheet" href="Assets/Css/default.css">
<link rel="stylesheet" href="User/Css/user.css">
<title><bean:write name="user" property="firstName" /> <bean:write
		name="user" property="lastName" /></title>
</head>
<body>
	<jsp:include page="/Assets/Templates/header.jsp"></jsp:include>

	<bean:define id="address" name="user" property="address"></bean:define>

	<main class="__pad-t-32">
	<div>
		<div class="__cwid">
			<div class="__row">
				<div class="__col l8">
					<div class=" __white __pad1624">
						<div class="__row __pad-b-10 ">
							<div class="__left">
								<div class="__pad-b-12">
									<logic:empty name="user" property="avatar">
										<img alt="" src="Assets/Img/anonymous.png" class="__img" />
									</logic:empty>
									<logic:notEmpty name="user" property="avatar">
										<bean:define id="userAvatar" name="user" property="avatar"></bean:define>
										<img alt="" src="User/Img/${ userAvatar }" class="__img" />
									</logic:notEmpty>
								</div>
								<div>
									<center>
										<span>ID </span>
										<bean:write name="user" property="id" format="" />
									</center>
								</div>
							</div>
							<div class="__left">
								<div class="__mar-l-12">
									<div class="__pad-t-12">
										<strong> <span> <bean:write name="user"
													property="firstName" /> <bean:write name="user"
													property="lastName" />
										</span>
										</strong>
									</div>
									<div class="__mar-t-8">
										<bean:write name="user" property="title"></bean:write>
									</div>
									<div class="__mar-t-8">
										<span class="__dcolor"> <i class="fa fa-map-marker"
											aria-hidden="true"></i> <bean:write name="address"
												property="name" />
										</span>
									</div>
									<div class="__mar-t-8">
										<logic:iterate id="ability" name="user" property="abilities">
											<bean:define id="abilityID" name="ability" property="id"></bean:define>
											<div class="__il-b">
												<a href="Search.do?abilityID=${ abilityID }"> <span
													class="__tag"><bean:write name="ability"
															property="name" /></span>
												</a>
											</div>

										</logic:iterate>
									</div>
								</div>
							</div>
						</div>
						<div>
							<div>
								<h4 class="__mar-v-10">Giới thiệu</h4>
							</div>
							<p>
								<bean:write name="user" property="description" />
							</p>
						</div>
					</div>

					<div class="__mar-t-8">
						<div class="__white __pad816">
							<h4>Công Việc</h4>
							<div>
								<ul>
									<logic:iterate id="job" name="user" property="jobs">
										<bean:define id="jobID" name="job" property="id"></bean:define>
										<li class="__pad-t-16"><a
											href="JobDetail.do?Job=${ jobID }">
												<div class="__row">
													<div class="__left">
														<img alt="" src="User/Img/${ avatar }" class="__avt">
													</div>
													<div class="__left __pad-l-16">
														<div class="__jtitle">
															<h5>
																<bean:write name="job" property="name"></bean:write>
															</h5>
														</div>
														<div>
															<bean:write name="job" property="money" format=""></bean:write>
														</div>
													</div>
												</div>
										</a></li>
									</logic:iterate>
								</ul>

							</div>

						</div>
					</div>
				</div>
				<div class="__col l4 __pad-l-32">
					<div class="__white">
						<div class="__pad816">
							<h5>Tóm lược</h5>
							<div class="__mar-t-8">
								<ul>
									<li>
										<div>Đã đăng</div>

									</li>
									<li>
										<div>Đã nạp đơn</div>
									</li>
								</ul>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	</main>
	<script src="Assets/Js/jquery-3.2.1.min.js"></script>
	<script src="Assets/Js/default.js"></script>

</body>
</html>