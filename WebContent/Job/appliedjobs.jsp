
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="Assets/Css/library.css">
<link rel="stylesheet" href="Assets/Css/font-awesome.min.css">
<link rel="stylesheet" href="Assets/Css/default.css">
<link rel="stylesheet" href="Job/Css/yourjob.css">
<title>Your Job</title>
</head>
<body>
	<jsp:include page="/Assets/Templates/header.jsp"></jsp:include>
	<main class="__pad-t-32">
	<div class="__cwid">
		<div>
			<bean:define id="userID" name="User" property="id"></bean:define>
			<div class="__mar-b-32">
				<div class="__white"
					style="border: 1px solid #efefef; border-top: 0;">
					<div style="padding-top: 4px;"></div>
					<ul class="__pad-h-12 __hei42">
						<li class="__il-b __hopt"><div>
								<a class="__hbut __pad-v-8 __ft-seui" href="Jobs.do"> Tuyển
									dụng </a>
							</div></li>
						<li class="__il-b __hopt __ac"><div>
								<a class="__hbut __pad-v-8 __ft-seui" href="ListAppliedJob.do">
									Đơn xin việc </a>
							</div></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="__row">
			<div class="__col l7">
				<div>
					<ul class="__white __bor">
						<logic:iterate id="lj" name="fListJob" property="listJob">
							<bean:define id="job" name="lj" property="job"></bean:define>
							<bean:define id="jobID" name="job" property="id"></bean:define>
							<li class="__bpad">
								<div>
									<div class="__row">
										<div class="__col l8 __pad-r-24">
											<div class="__pad-l-16">
												<h4 class="__pad-b-8">
													<a href="JobDetail.do?Job=${ jobID }" class="__color">

														<bean:write name="job" property="name" />
													
													</a>
												</h4>
											</div>

											<div class="">
												<div class="__pad816 __bkcolor">
													<span>Giá tiền yêu cầu</span> <span><strong><bean:write
																name="lj" property="money" format="" /></span></strong>
												</div>
											</div>
											<div class="__pad-l-16 __mar-v-8">
												<h6>Mô tả</h6>
												<div class="__pad-t-4">
													<span><bean:write name="lj" property="description" /></span>
												</div>
											</div>
											<div class="__pad-l-16">
												<h6>Thuyết Phục</h6>
												<div class="__pad-t-4">
													<span><bean:write name="lj" property="convince" /></span>
												</div>
											</div>
										</div>
										<div class="__col l4">
											<div class="__tright __mar-b-8">
												<div class="__il-b">
													<div class="__tright">
														<span><bean:write name="lj" property="applyTime"
																format="MM-dd-yyyy" /></span>
													</div>
												</div>
												<div class="__il-b __rlt">
													<button class="__but __boptions">
														<i class="fa fa-ellipsis-v" aria-hidden="true"></i>
													</button>
													<div class="__abs __options">
														<ul class="__card-2 __white __wid186">
															<li class="__center"><a class="__but"
																href="CancelAppliedJob.do?userID=${ userID }&jobID=${ jobID }">Chỉnh
																	sửa đơn</a></li>
															<li class="__center"><a class="__but"
																href="CancelAppliedJob.do?userID=${ userID }&jobID=${ jobID }">Xóa
																	đơn</a></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="__pad-r-16">
												<div class="__pad-b-8 __right-align">
													<logic:equal value="1" name="lj" property="isApplied" >
														<span class="__color">Đơn đã được chấp nhận</span>
													</logic:equal>
													<logic:equal value="2" name="lj" property="isApplied" >
														<span class="__color">Đơn đã bị từ chối</span>
													</logic:equal>
												</div>
											</div>
											<div class="__row __lbcolor __uspf">
												<div class="__left">
													<div class=" __dcolor">
														<div>

															<div>
																<div>
																	<i class="fa fa-phone __w20" aria-hidden="true"></i><span><bean:write
																			name="lj" property="phone" /></span>
																</div>
																<div class="__pad-v-8">
																	<a
																		href="<bean:write
																				name="lj" property="facebook" />"><i
																		class="fa fa-facebook __w20" aria-hidden="true"></i><span><bean:write
																				name="lj" property="facebook" /></span></a>
																</div>
																<div>
																	<a
																		href="mailto:<bean:write
																				name="lj" property="email" />">
																		<i class="fa fa-envelope-open-o __w20"
																		aria-hidden="true"></i><span><bean:write
																				name="lj" property="email" /></span>
																	</a>

																</div>
															</div>


														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
							</li>
						</logic:iterate>

					</ul>
				</div>
			</div>
			<div class="__col l5"></div>
		</div>

	</div>
	</main>
	<script type="text/javascript" src="Assets/Js/jquery-3.2.1.js"></script>

	<script type="text/javascript" src="Assets/Js/default.js"></script>
	<script type="text/javascript" src="Job/Js/yourjob.js"></script>
</body>
</html>