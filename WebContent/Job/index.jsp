<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="Assets/Css/library.css">
<link rel="stylesheet" href="Assets/Css/font-awesome.min.css">
<link rel="stylesheet" href="Assets/Css/default.css">
<link rel="stylesheet" href="Job/Css/job.css">
<title>Job Web</title>

</head>
<body>
	<jsp:include page="/Assets/Templates/header.jsp"></jsp:include>
	<main class="__pad-t-32">
	<div class="__cwid" id="Container">
		<logic:notEmpty name="fJobDetail" property="job">
			<div class="__row __mar-b-24">
				<div class="__col l9 __pad-28p">
					<bean:define id="job" name="fJobDetail" property="job"></bean:define>
					<bean:define id="user" name="job" property="user"></bean:define>
					<bean:define id="jobID" name="job" property="id"></bean:define>
					<bean:define id="jobRequirement" name="job"
						property="jobRequirement"></bean:define>
					<bean:define id="userID" name="user" property="id" />
					<div class="__white __bor">
						<ul class="__pad816">
							<li>
								<div class="__title">
									<div class="__row">
										<div class="__col l9">
											<h3>
												<bean:write name="job" property="name" />
											</h3>
										</div>
										<div class="__col l3">
											<h3>
												<bean:write name="job" property="created" format="" />
											</h3>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="__pad-b-12">
									<bean:write name="job" property="description" />
								</div>
							</li>
							<li>
								<div>
									<ul>
										<li class="__il-b">Kỹ năng</li>
										<logic:iterate id="ability" name="jobRequirement">
											<bean:define id="abilityID" name="ability" property="id"></bean:define>
											<li class="__il-b"><a
												href="Search.do?abilityID=${ abilityID }" class="__tag"><bean:write
														name="ability" property="name" /></a></li>
										</logic:iterate>

									</ul>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="__col l3 ">
					<div>
						<logic:notEmpty name="User">
							<logic:equal value="${ userID }" name="User" property="id">
								<div class="__white __pad816 __mar-b-16 __bor">
									<ul class="__ul">
										<logic:notEmpty name="fJobDetail" property="isPublish">

											<logic:equal value="0" name="fJobDetail" property="isPublish">
												<li class="__pad-v-8 __color">Dự án đã được bắt đầu</li>
											</logic:equal>
											<logic:notEqual value="0" name="fJobDetail"
												property="isPublish">
												<logic:greaterThan value="0" name="fJobDetail"
													property="appliedNumber">
													<li class="__pad-v-8"><a
														href="StartJob.do?jobID=${ jobID }" class="">Bắt đầu
															dự án</a></li>
												</logic:greaterThan>
											</logic:notEqual>
										</logic:notEmpty>

										<li class="__pad-v-8"><a
											href="EditJob.do?jobID=${ jobID }" class="">Sửa bài đăng</a></li>
										<li class="__pad-v-8"><a
											href="DeleteJob.do?jobID=${ jobID }" class="">Xóa bài
												đăng</a></li>
									</ul>
								</div>
							</logic:equal>
						</logic:notEmpty>
						<div class="__white __pad816 __mar-b-16 __bor">
							<div>
								<h5 class="__mar-v-12">Thông tin dự án</h5>
							</div>
							<ul>
								<li class="__pad-b-12">
									<div class="__il-b __wid80 __des-job">ID Dự Án</div>
									<div class="__il-b">
										<bean:write name="job" property="id" format="" />
									</div>
								</li>
								<li class="__pad-b-12">
									<div class="__il-b __wid80 __des-job">Hạn đến</div>
									<div class="__il-b">
										<bean:write name="job" property="applicationExpire"
											format="MM-dd-yyyy" />
									</div>
								</li>
								<li class="__pad-b-12"><bean:define id="address" name="job"
										property="address"></bean:define>
									<div class="__il-b __wid80 __des-job">Địa điểm</div>
									<div class="__il-b">
										<bean:write name="address" property="name" />
									</div></li>
								<li class="__pad-b-12">
									<div class="__il-b __wid80 __des-job">Ngân sách</div>
									<div class="__il-b">
										<bean:write name="job" property="money" format="" />
									</div>
								</li>
							</ul>
						</div>
						<div class="__white __pad816 __bor">
							<bean:define id="recruitment" name="job" property="user"></bean:define>
							<div>
								<h5>Thông tin nhà tuyển dụng</h5>
							</div>
							<ul>
								<li style="padding-bottom: 4px;">
									<div class="__pad-v-12">
										<div class="__row">
											<div class="__left">
												<logic:empty name="recruitment" property="avatar">
													<img alt="" src="Assets/Img/anonymous.png" class="__avatar" />
												</logic:empty>
												<logic:notEmpty name="recruitment" property="avatar">

													<bean:define id="userAvatar" name="recruitment"
														property="avatar"></bean:define>
													<img alt="" src="User/Img/${ userAvatar }" class="__avatar" />
												</logic:notEmpty>

											</div>
											<div class="__left">
												<a href="Profile.do?User=${ userID }" id="Avt"
													class="__color __pad-l-12 __uinfo"> <bean:write
														name="recruitment" property="firstName" /> <bean:write
														name="recruitment" property="lastName" /></a>
											</div>
										</div>

									</div>
								</li>
								<li class="__pad-b-12">
									<div class="__il-b __wid80 __des-job">Đến từ</div>
									<div class="__il-b">
										<bean:define id="uaddress" name="user" property="address"></bean:define>
										<bean:write name="uaddress" property="name" />
									</div>
								</li>
								<li class="__pad-b-12">
									<div class="__il-b __wid80 __des-job">Tham gia</div>
									<div class="__il-b">
										<bean:write name="user" property="created" format="MM-dd-yyyy" />
									</div>
								</li>
								<li class="__pad-b-12">
									<div class="__il-b __wid80 __des-job">Nạp đơn</div>
									<div class="__il-b">
										<bean:write name="user" property="jobApplied" format="" />
										việc
									</div>
								</li>
								<li class="__pad-b-12">
									<div class="__il-b __wid80 __des-job">Đã đăng</div>
									<div class="__il-b">
										<bean:write name="user" property="jobRecruit" format="" />
										việc
									</div>
								</li>
							</ul>
						</div>

					</div>
				</div>
			</div>
			<logic:notEmpty name="User">
				<logic:notEqual value="${ userID }" name="User" property="id">
					<logic:notEqual value="1" name="fJobDetail" property="applied">

						<bean:define id="sessionUser" name="User" property="id"></bean:define>
						<div class="__mar-b-16 __white __pad816 __bor" id="EditForm">
							<form action="ApplyJob.do" method="POST">
								<input type="hidden" value="${ sessionUser }" name="userID">
								<input type="hidden" value="${ jobID }" name="jobID">
								<h4 class="__tconv __mar-v-12">Nạp đơn xin việc</h4>
								<hr class="__hr" />
								<div class="__tconv __pad-b-12">
									<strong>Đề xuất chi phí</strong>
								</div>
								<div class="__pad-b-24">
									<div class="__row">
										<div class="__col l4">
											<div class="__bmoney __mar-b-12 ">
												<ul class="__pad-v-8 __pad-h-16 __row">
													<li class="__col l6">
														<div class="__il-b __wid150">
															<h5>Giá đề xuất</h5>
														</div>

													</li>
													<li class="__col l6">

														<div class="__il-b">
															<input type="number" id="iMoney" name="money"
																placeholder="20.000.000">
														</div>
													</li>
												</ul>
											</div>
											<div class="__pad-b-12">
												<div class="__tconv __pad-b-12">
													<strong>Dự kiến hoàn thành trong</strong>
												</div>
												<div class="__row">
													<div class="__left __bbor">
														<div class="__il-b __pad-8">
															<input type="number" name="finishedIn" id="iExpire"
																placeholder="10" class="__center __w261">
														</div>
														<div class="__il-b">
															<select class="__select ">
																<option value="0">Ngày</option>
															</select>
														</div>
													</div>
												</div>
											</div>
											<div class="__mar-b-16">
												<div class="__tconv __pad-b-12">
													<strong>Thông tin liên hệ của bạn</strong>
												</div>
												<div class="__row">
													<div class="">
														<input type="number" class="__txa"
															placeholder="01648448898" name="phone"
															required="required" />
													</div>
													<div class="__pad-v-12">
														<input type="link" placeholder="facebook.com/gvr.37"
															class="__txa" name="facebook" />
													</div>
													<div class="">
														<input type="email" placeholder="gnt.v37@gmail.com"
															class="__txa" name="email" required="required" />
													</div>
												</div>

											</div>
										</div>
										<div class="__col __pad-l-48 l8">

											<div class="">
												<div class="__tconv __pad-b-12">
													<strong>Đề xuất thuyết phục nhà tuyển dụng</strong>
												</div>
												<ul>
													<li><span class="__pad-v-8">1. Kỹ năng phù hợp</span>
														<div class=" __pad-t-16 __pad-b-8">

															<bean:define id="abilities" name="fJobDetail"
																property="abilities"></bean:define>
															<select class="__select" id="AbilityOptions">
																<option selected="selected" disabled="disabled">-
																	Chọn kỹ năng -</option>
																<logic:iterate id="ability" name="abilities">
																	<bean:define id="abilityID" name="ability"
																		property="id"></bean:define>
																	<bean:define id="abilityName" name="ability"
																		property="name"></bean:define>
																	<option value="${ abilityID }" key="${ abilityName }">-
																		<bean:write name="ability" property="name" /> -
																	</option>
																</logic:iterate>
															</select> <div class="__pad-t-8"> <input type="hidden"
																name="abilities" id="Abilities" />
															<div id="ShowAblities"></div>
														</div>
											</div>
											</li>
											<li><span>2. Bạn có kinh nghiệm và kỹ năng nào
													phù hơn với dự án này?</span>
												<div class="__pad-v-16">
													<textarea
														placeholder="Tôi có kinh nghiệm trong lĩnh vực..."
														class="__txa" name="convince" id="iConvince"></textarea>
												</div></li>
											<li><span>3. Bạn dự định thực hiện dự án này như
													thế nào?</span>
												<div class="__pad-v-16">
													<textarea placeholder="Đầu tiên tôi sẽ..." class="__txa"
														name="description" id="iDescription"></textarea>
												</div></li>
											</ul>
										</div>
										<div class="__right-align">
											<button class="__bbut">Nạp đơn</button>
										</div>
									</div>
								</div>
						</div>
						</form>
	</div>
	</logic:notEqual> </logic:notEqual> </logic:notEmpty>

	<div class="__row">
		<div class="__col l9 __pad-28p">
			<div class="__white __bor">
				<div id="AppiedUserList" class="">
					<logic:iterate id="applyJob" name="fJobDetail"
						property="userAppliedList">
						<bean:define id="userApply" name="applyJob" property="user"></bean:define>
						<bean:define id="userApplyID" name="userApply" property="id"></bean:define>

						<section class="__row __bbor __pad816">
						<div class="__col l1 __mar-t-10">
							<div class="">
								<logic:empty name="userApply" property="avatar">
									<a href="Profile.do?User=${ userApplyID }" class="__color"><img
										alt="" src="Assets/Img/anonymous.png" class="__image"></a>
								</logic:empty>
								<logic:notEmpty name="userApply" property="avatar">
									<bean:define id="userApplyAvatar" name="userApply"
										property="avatar"></bean:define>
									<a href="Profile.do?User=${ userApplyID }" class="__color"><img
										alt="" src="User/Img/${ userApplyAvatar }" class="__image"></a>
								</logic:notEmpty>
							</div>
						</div>
						<div class="__col l11 __mar-t-10 __pad-b-8 __pad-l-24">
							<div class="__row">
								<div class="__col l8">
									<div>
										<div class="__il-b">
											<a href="Profile.do?User=${ userApplyID }" class="__color">
												<bean:write name="userApply" property="firstName" /> <bean:write
													name="userApply" property="lastName" />
											</a>
										</div>
										<div class="__il-b __pad-l-8">
											<logic:notEmpty name="User">
												<bean:define id="sessionUser" name="User" property="id"></bean:define>
												<logic:equal value="${ sessionUser }" name="user"
													property="id">
													<button class="__but __contact"
														onclick="document.getElementById('Contact').style.display='block'">Liên
														hệ</button>
													<modal id="Contact" class="__modal"> <modal-content
														class="__modal-content __animate-opacity __card-4">
													<div class="__container __teal">
														<span
															onclick="document.getElementById('Contact').style.display='none'"
															class="__but __display-topright __f18">&times;</span>
														<h4 class="__pad-1016">Thông tin liên hệ</h4>
													</div>
													<div class="__container __bkcolor __pad-h-16">
														<logic:equal value="${ userID }" name="user" property="id">
															<div class="__pad-v-8">
																<div class="__pad-t-12 __row">
																	<div class="__left">
																		<span class="__il-b" style="width: 16px;"> <i
																			class="fa fa-phone" aria-hidden="true"></i>
																		</span> <span class="__pad-h-8">Số điện thoại</span>
																	</div>
																	<div>
																		<span><bean:write name="applyJob"
																				property="phone" /></span>
																	</div>

																</div>
																<div class="__row __pad-v-12">
																	<bean:define id="facebook" name="applyJob"
																		property="facebook"></bean:define>

																	<div class="__left">
																		<span class="__il-b" style="width: 19px;"><i
																			class="fa fa-facebook" aria-hidden="true"></i></span><span
																			class="__pad-h-8">Facebook</span>
																	</div>
																	<div class="__left">
																		<a href="${ facebook }" target="blank"
																			class="__color __td">${ facebook }</a>
																	</div>
																</div>
																<div class="__row __pad-b-12">
																	<bean:define id="email" name="applyJob"
																		property="email"></bean:define>
																	<div class="__left">
																		<span><i class="fa fa-envelope-open-o"
																			aria-hidden="true"></i> </span> <span class="__pad-h-8">Email</span>
																	</div>
																	<div class="__left">
																		<a href="mailto:${ email }" target="blank"
																			class="__color __td"><bean:write name="applyJob"
																				property="email" /></a>
																	</div>

																</div>
															</div>
														</logic:equal>
													</div>

													</modal-content> </modal>
												</logic:equal>

											</logic:notEmpty>
										</div>
									</div>
									<div class="__pad-v-8">
										<span>Giá yêu cầu</span><span class=""> <bean:write
												name="applyJob" property="money" format="" /></span>
									</div>
									<div>
										<span class="Description"> <bean:write name="applyJob"
												property="description" /></span>
									</div>
									<div class="__pad-v-8">
										<span class="Convince"><bean:write name="applyJob"
												property="convince" /></span>
									</div>

									<div class="__hide">
										<div class="">

											<input type="hidden" class="Money"
												value="<bean:write
														name="applyJob" property="money" format="0" />">

											<input type="hidden" class="Description"
												value="<bean:write
														name="applyJob" property="description" />">

											<input type="hidden" class="Convince"
												value="<bean:write
														name="applyJob" property="convince" />">

											<input type="hidden" class="Phone"
												value="<bean:write
														name="applyJob" property="phone" format="0" />">

											<input type="hidden" class="Facebook"
												value="<bean:write
														name="applyJob" property="facebook" />">

											<input type="hidden" class="Email"
												value="<bean:write
														name="applyJob" property="email" />" />

										</div>
										<div>
											<input type="hidden" class="AppliedUser"
												value="${ userApplyID }" /> <input type="hidden"
												class="Job" value="${ jobID }" />
										</div>

									</div>
								</div>
								<div class="__col l4">
									<div></div>
									<div class="__right-align">
										<div class="__il-b">
											<time class="timeago __pad-r-16"
												datetime="<bean:write name="applyJob" property="applyTime" format="MM-dd-yyy HH:mm:ss" />">
											<bean:write name="job" property="created"
												format="MM-dd-yyy hh:mm:ss" /></time>
										</div>
										<logic:equal value="0" name="applyJob" property="isApplied">
											<div class="__il-b __rlt">
												<button id="UserJobOptions" class="__but">
													<i class="fa fa-ellipsis-v" aria-hidden="true"></i>
												</button>
												<div class="__abs __posi" style="display: none;">
													<ul class="__ul __card-2 __white __w186">
														<logic:notEmpty name="User">
															<logic:equal value="${ userID }" name="User"
																property="id">

																<li class="__left-align __pad816">
																	<form action="AcceptUser.do" method="post">
																		<input type="hidden" name="userID"
																			value="${ userApplyID }" /> <input type="hidden"
																			name="jobID" value="${ jobID }" />
																		<button class="__ft-seui __f15">Chấp nhận đơn</button>
																	</form>
																</li>
																<li class="__left-align __pad816"><a
																	href="CancelUser.do?userID=${ userApplyID }&jobID=${ jobID }">Từ
																		chối đơn</a></li>
															</logic:equal>
															<logic:equal value="${ userApplyID }" name="User"
																property="id">
																<li class="__left-align __pad816" id="UserJobEdit"><a>Chỉnh
																		sửa đơn</a></li>
																<li class="__left-align __pad816"><a
																	class="__ft-seui __f15"
																	href="CancelAppliedJob.do?userID=${ userApplyID }&jobID=${ jobID }">Xóa
																		đơn</a></li>
															</logic:equal>
														</logic:notEmpty>
													</ul>
												</div>
											</div>
										</logic:equal>

										<logic:equal value="1" name="applyJob" property="isApplied">
											<div class="__pad-r-16 __pad-v-8 __color">Đã chấp nhận
												đơn</div>
										</logic:equal>

										<logic:equal value="2" name="applyJob" property="isApplied">
											<div class="__pad-r-16 __pad-v-8 __color">Không chấp
												nhận đơn</div>
										</logic:equal>

										<div class="__pad-r-16">
											<span>Hoàn thành trong</span> <span class="Expire"> <bean:write
													name="applyJob" property="finishedIn" />
											</span>
											<span>ngày</span>
										</div>
									</div>
								</div>
							</div>

							<div>
								<logic:notEmpty name="userApply" property="abilities">
									<ul>
										<li class="__il-b">Kỹ Năng</li>
										<logic:iterate id="ujAbility" name="userApply"
											property="abilities">
											<li class="__il-b"><a class="__tag"><bean:write
														name="ujAbility" property="name" /></a></li>
										</logic:iterate>
									</ul>

								</logic:notEmpty>

							</div>
						</div>

						</section>

					</logic:iterate>
				</div>
			</div>
		</div>
	</div>
	</logic:notEmpty> <logic:empty name="fJobDetail" property="job">
		<div class="__center">
			<h4>Công việc này không còn tồn tại hoặc bạn không có quyền truy
				cập</h4>
		</div>
	</logic:empty>

	</div>
	</main>
	<script type="text/javascript" src="Assets/Js/jquery-3.2.1.js"></script>
	<script src="Home/Js/jquery.timeago.js" type="text/javascript"></script>
	<script src="Home/Js/jquery.timeago.vi.js" type="text/javascript"></script>

	<script src="Job/Js/autosize.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery(document).ready(function() {
				$("time.timeago").timeago();
			});

		});
	</script>
	<script src="Assets/Js/default.js"></script>
	<script src="Job/Js/job.js"></script>

	<script src="Job/Js/recruitment.js"></script>
</body>
</html>