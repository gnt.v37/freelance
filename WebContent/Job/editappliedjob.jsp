<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="Assets/Css/library.css">
<link rel="stylesheet" href="Assets/Css/font-awesome.min.css">
<link rel="stylesheet" href="Assets/Css/default.css">
<link rel="stylesheet" href="Job/Css/job.css">
<title>Job Web</title>

</head>
<body>
	<jsp:include page="/Assets/Templates/header.jsp"></jsp:include>
	<main class="__pad-t-32">
	<div class="__cwid">
		<
		<logic:notEqual value="${ userID }" name="User" property="id">
			<bean:write name="fJobDetail" property="applied" format="" />
			<logic:notEqual value="1" name="fJobDetail" property="applied" >
			
				<bean:define id="sessionUser" name="User" property="id"></bean:define>
				<div class="__mar-b-16 __white __pad816 __bor">
					<form action="ApplyJob.do" method="POST">
						<input type="hidden" value="${ sessionUser }" name="userID">
						<input type="hidden" value="${ jobID }" name="jobID">
						<h4 class="__tconv __mar-v-12">Nạp đơn xin việc</h4>
						<hr class="__hr" />
						<div class="__tconv __pad-b-12">
							<strong>Đề xuất chi phí</strong>
						</div>
						<div class="__pad-b-24">
							<div class="__bmoney __mar-b-12 ">
								<ul class="__pad-v-8 __pad-h-16 __row">
									<li class="__col l4">
										<div class="__il-b __wid150">
											<h5>Giá đề xuất</h5>
										</div>
										<div class="__il-b">
											<input type="number" name="money" placeholder="20.000.000">
										</div>
									</li>
								</ul>
							</div>
							<div class="__pad-b-12">
								<div class="__tconv __pad-b-12">
									<strong>Dự kiến hoàn thành trong</strong>
								</div>
								<div class="__row">
									<div class="__left __bbor">
										<div class="__il-b __pad-8">
											<input type="number" name="finishedIn" placeholder="10"
												class="__center">
										</div>
										<div class="__il-b">
											<select class="__select ">
												<option value="0">Ngày</option>
												<option value="1">Tuần</option>
												<option value="2">Tháng</option>
												<option value="3">Năm</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div>
								<div class="__tconv __pad-b-12">
									<strong>Đề xuất thuyết phục khách hàng</strong>
								</div>
								<ul>
									<li><span>1. Bạn có kinh nghiệm và kỹ năng nào phù
											hơn với dự án này?</span>
										<div class="__pad-v-16">
											<textarea placeholder="Tôi có kinh nghiệm trong lĩnh vực..."
												class="__txa" name="convince"></textarea>
										</div></li>
									<li><span>2. Bạn dự định thực hiện dự án này như
											thế nào?</span>
										<div class="__pad-v-16">
											<textarea placeholder="Đầu tiên tôi sẽ..." class="__txa"
												name="description"></textarea>
										</div></li>
								</ul>
							</div>
							<div class="__mar-b-16">
								<div class="__tconv __pad-b-12">
									<strong>Thông tin liên hệ của bạn</strong>
								</div>
								<div class="__row">
									<div class="__col l4">
										<input type="number" class="__txa" placeholder="01648448898"
											name="phone" />
									</div>
									<div class="__col l4 __pad-l-12">
										<input type="text" placeholder="facebook.com/gvr.37"
											class="__txa" name="facebook" />
									</div>
									<div class="__col l4 __pad-l-12">
										<input type="text" placeholder="gnt.v37@gmail.com"
											class="__txa" name="email" />
									</div>
								</div>

							</div>
							<div class="__right-align">
								<button class="__bbut">Nạp đơn</button>
							</div>
						</div>
					</form>
				</div>
			</logic:notEqual>
		</logic:notEqual>
		<div class="__row">
			<div class="__col l9 __pad-28p">
				<div class="__white __bor">
					<div class="">

						<logic:iterate id="applyJob" name="fJobDetail"
							property="userAppliedList">
							<bean:define id="userApply" name="applyJob" property="user"></bean:define>
							<bean:define id="userApplyID" name="userApply" property="id"></bean:define>
							<bean:define id="userApplyAvatar" name="userApply"
								property="avatar"></bean:define>
							<div class="__row __bbor __pad816">
								<div class="__col l1 __mar-t-10">
									<div class="">
										<a href="Profile.do?User=${ userApplyID }" class="__color"><img
											alt="" src="User/Img/${ userApplyAvatar }" class="__image"></a>
									</div>
								</div>
								<div class="__col l11 __mar-t-10 __pad-b-16 __pad-l-24">
									<div class="__row">
										<div class="__col l8">
											<div>
												<a href="Profile.do?User=${ userApplyID }" class="__color">
													<bean:write name="userApply" property="firstName" /> <bean:write
														name="userApply" property="lastName" />
												</a>
											</div>
											<div class="__pad-v-8">
												<span>Giá yêu cầu</span><span class=""> <bean:write
														name="applyJob" property="money" format="" /></span>
											</div>
											<div >
												<span> <bean:write name="applyJob"
														property="description" /></span>
											</div>
											<div class="__pad-v-8">
												<span><bean:write name="applyJob" property="convince" /></span>
											</div>
										</div>
										<div class="__col l4">
											<div></div>
											<div class="__right-align">
												<div class="__il-b">
													<span class="__pad-r-16"><bean:write name="applyJob"
															property="applyTime" format="MM-dd-yyyy" /></span>
												</div>
												<logic:equal value="0" name="applyJob" property="isApplied">
													<div class="__il-b __rlt">
														<button id="UserJobOptions" class="__but">
															<i class="fa fa-ellipsis-v" aria-hidden="true"></i>
														</button>
														<div class="__abs __posi" style="display: none;">
															<ul class="__ul __card-2 __white __w186">
																<logic:equal value="${ userID }" name="User"
																	property="id">

																	<li class="__left-align __pad816">
																		<form action="AcceptUser.do" method="post">
																			<input type="hidden" name="userID"
																				value="${ userApplyID }" /> <input type="hidden"
																				name="jobID" value="${ jobID }" />
																			<button class="__ft-seui __f15">Chấp nhận đơn</button>
																		</form>
																	</li>
																	<li class="__left-align __pad816"><a
																		href="CancelUser.do?userID=${ userApplyID }&jobID=${ jobID }">Từ
																			chối đơn</a></li>
																</logic:equal>
																<logic:equal value="${ userApplyID }" name="User"
																	property="id">
																	<li class="__left-align __pad816"><a>Chỉnh sửa đơn</a></li>
																	<li class="__left-align __pad816"><a class="__ft-seui __f15"
																		href="CancelAppliedJob.do?userID=${ userApplyID }&jobID=${ jobID }">Xóa
																			đơn</a></li>
																</logic:equal>
															</ul>
														</div>
													</div>
												</logic:equal>
												
												<logic:equal value="1" name="applyJob" property="isApplied">
													<div class="__pad-r-16 __pad-v-8 __color">Đã chấp nhận đơn</div>
												</logic:equal>
												
												<logic:equal value="2" name="applyJob" property="isApplied">
													<div class="__pad-r-16 __pad-v-8 __color">Không chấp nhận đơn</div>
												</logic:equal>
												
												<div class="__pad-r-16">
													<span >Hoàn thành trong</span> <span> <bean:write
															name="applyJob" property="finishedIn" /></span>
												</div>
											</div>
										</div>
									</div>
									<ul class="__row">
										<li class="__col l8"></li>
										<li class="__col l4 __right-align"></li>
									</ul>
									<ul class="__row __pad-t-8">
										<li class="__col l8"></li>
										<li class="__col l4 __right-align">
											<div class="__pad-r-16"></div>
										</li>
									</ul>

								</div>
							</div>

						</logic:iterate>
					</div>
				</div>
			</div>
		</div>


	</div>
	</main>
	<script type="text/javascript" src="Assets/Js/jquery-3.2.1.js"></script>
	<script src="Assets/Js/default.js"></script>
	<script src="Job/Js/job.js"></script>
</body>
</html>