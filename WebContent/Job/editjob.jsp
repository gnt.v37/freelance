<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="Assets/Css/library.css">
<link rel="stylesheet" type="text/css"
	href="Assets/Css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="Assets/Css/default.css">
<link rel="stylesheet" type="text/css" href="Job/Css/recruitment.css">
<title>Recruitment</title>
</head>
<body>
	<jsp:include page="/Assets/Templates/header.jsp"></jsp:include>
	<div class="__mar-t-32">


		<form action="EditProcess.do" method="POST">
			<div class="__width __white __bor">
				<div class="__pad">
					<center>
						<h1>Cập nhật công việc</h1>
					</center>
					<div class="__pad-t-42">
						<bean:define id="userID" name="User" property="id"></bean:define>
						<bean:define id="job" name="fEditJobPage" property="job"></bean:define>
						<bean:define id="jobID" name="job" property="id"></bean:define>
						<bean:define id="jobName" name="job" property="name"></bean:define>
						<bean:define id="jobMoney" name="job" property="money"></bean:define>
						<bean:define id="jobDescription" name="job" property="description"></bean:define>

						<input type="hidden" class="__hide" name="jobID"
							value="${ jobID }">
						<ul class="__row __pad-b-12">
							<li class="__col l2"><div class="__icon"></div></li>
							<li class="__col l10">
								<div class="__mar-b-10">
									<h4>Công việc bạn cần làm</h4>
								</div>
								<h6 class="__pad-b-8">Chọn lĩnh vực cho công việc này</h6>
								<div>
									<bean:define id="categories" name="fEditJobPage"
										property="categories"></bean:define>

									<select class="__select" name="categoryID">
										<logic:iterate id="category" name="categories">
											<bean:define id="categoryID" name="category" property="id"></bean:define>
											<option value="${ categoryID }"><bean:write
													name="category" property="name" /></option>
										</logic:iterate>

									</select>
								</div>
								<h6 class="__pad-v-8">Đặt tên cho công việc</h6> <input
								class="__inp" type="text"
								placeholder="VD: Thiết kế trang web bán hàng"
								required="required" name="jobName" value="${ jobName }" />
							</li>
						</ul>
						<ul class="__row __pad-v-12">
							<li class="__col l2"><div class="__icon __i2"></div></li>
							<li class="__col l10">
								<div class="__mar-b-10">
									<h4>Nói với người xin việc về công việc</h4>
								</div>
								<h6 class="__pad-b-8">Các đầu việc cần làm</h6> <textarea
									class="__inp"
									placeholder="Ví dụ: Các giao diện website cần thiết kế trang chủ, xem hàng, thanh toán"
									required="required" name="jobDescription"
									value=${ jobDescription }>${ jobDescription }</textarea>
								<h6 class="__pad-v-8">Kỹ năng phù hợp</h6> <bean:define
									id="abilities" name="fEditJobPage" property="abilities"></bean:define>

								<select class="__select" id="AbilityOptions">
									<option selected="selected" disabled="disabled">- Chọn
										kỹ năng -</option>
									<logic:iterate id="ability" name="abilities">
										<bean:define id="abilityID" name="ability" property="id"></bean:define>
										<bean:define id="abilityName" name="ability" property="name"></bean:define>
										<option value="${ abilityID }" key="${ abilityName }">-
											<bean:write name="ability" property="name" /> -
										</option>
									</logic:iterate>
							</select>

								<div class="__pad-t-8">
									<input type="hidden" name="abilities" id="Abilities" />
									<div id="ShowAblities"></div>
								</div>
								<h6 class="__pad-v-8">Hạn nhận hồ sơ xin việc</h6> <input
								type="date" placeholder="VD: 11/11/1995" class="__inp"
								name="jobApplicationExpire"
								value="<bean:write name="job" property="applicationExpire" format="yyyy-MM-dd"/>" />

							</li>
						</ul>
						<ul class="__row __pad-v-12">
							<li class="__col l2"><div class="__icon __i3"></div></li>
							<li class="__col l10">
								<div class="__mar-b-10">
									<h4>Yêu cầu</h4>
								</div>
								<h6 class="__pad-b-8">Cần thuê tại</h6>
								<div>
									<bean:define id="address" name="fEditJobPage"
										property="address"></bean:define>
									<select class="__select" name="addressID">
										<logic:iterate id="addr" name="address">
											<bean:define id="addressID" name="addr" property="id"></bean:define>
											<option value="${ addressID }">-
												<bean:write name="addr" property="name" /> -
											</option>
										</logic:iterate>
									</select>
								</div>
							</li>
						</ul>
						<ul class="__row __pad-v-12">
							<li class="__col l2"><div class="__icon __i4"></div></li>
							<li class="__col l10">
								<div class="__mar-b-10">
									<h4>Ngân sách dự kiến</h4>
								</div>
								<h6 class="__pad-b-8">Số tiền tối đa có thể trả</h6> <input
								type="number" placeholder="VD: 2.500.000" class="__inp"
								required="required" name="jobMoney" value="${ jobMoney }" />
							</li>
						</ul>
						<ul class="__row __pad-v-12">
							<li class="__col l2"><div class="__icon __i5"></div></li>
							<li class="__col l10">
								<div>
									<h3>Tùy chọn khác (Không bắt buộc)</h3>
								</div>
								<div class="__mar-t-10">
									<input type="checkbox" class="__checkb __left" />
									<p class="__mar-l-32">
										<i class="fa fa-lock __lock" aria-hidden="true"></i> <span>Đăng
											việc bí mật, không hiển thị công khai trong danh sách việc.</span>
									</p>
								</div>
							</li>
						</ul>
					</div>

					<div class="__pad-h-62 __mar-t-32">
						<div class="__row">
							<div class="__col l2 __mhei1"></div>
							<div class="__col l10">
								<button class="__but __bt">Cập nhật</button>
							</div>
						</div>

					</div>
				</div>

			</div>
		</form>
	</div>
	<jsp:include page="/Assets/Templates/footer.jsp"></jsp:include>
	<script src="Assets/Js/jquery-3.2.1.min.js"></script>
	<script src="Assets/Js/default.js"></script>
	<script src="Job/Js/autosize.min.js"></script>
	<script src="Job/Js/recruitment.js"></script>
</body>
</html>