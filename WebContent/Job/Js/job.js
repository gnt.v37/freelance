/**
 * 
 */
(function () {

class EditJob {
	constructor () {
		
	}
	
	getHTML () {
		return `<div class="__mar-b-16 __white __pad816 __bor __rlt" id="EditForm">
						<form action="EditAppliedJob.do" method="POST" >
							<input type="hidden" id="iUserAppliedID" value="" name="userID">
							<input type="hidden" id="iJobID" value="" name="jobID">
							<div>
								
								<h4 class="__tconv __mar-v-12">Nạp đơn xin việc</h4>
								<div class="__display-topright"><button type="button" id="CloseEditForm" class="__but __f18">&times;</button></div>
							</div>
							
							<hr class="__hr" />
							<div class="__tconv __pad-b-12">
								<strong>Đề xuất chi phí</strong>
							</div>
							<div class="__pad-b-24">
								<div class="__row">
									<div class="__col l4">
										<div class="__bmoney __mar-b-12 ">
											<ul class="__pad-v-8 __pad-h-16 __row">
												<li class="__col l6">
													<div class="__il-b __wid150">
														<h5>Giá đề xuất</h5>
													</div>
													
												</li>
												<li class="__col l6">
													
													<div class="__il-b">
														<input type="number" id="iMoney" name="money" placeholder="20.000.000">
													</div>
												</li>
											</ul>
										</div>
										<div class="__pad-b-12">
											<div class="__tconv __pad-b-12">
												<strong>Dự kiến hoàn thành trong</strong>
											</div>
											<div class="__row">
												<div class="__left __bbor">
													<div class="__il-b __pad-8">
														<input type="number" name="finishedIn" id="iExpire" placeholder="10"
															class="__center __w261">
													</div>
													<div class="__il-b">
														<select class="__select ">
															<option value="0">Ngày</option>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="__mar-b-16">
											<div class="__tconv __pad-b-12">
												<strong>Thông tin liên hệ của bạn</strong>
											</div>
											<div class="__row">
												<div class="">
													<input type="number" class="__txa"
														placeholder="01648448898" name="phone" id="iPhone" required="required" />
												</div>
												<div class="__pad-v-12">
													<input type="text" placeholder="facebook.com/gvr.37"
														class="__txa" name="facebook" id="iFacebook" />
												</div>
												<div class="">
													<input type="text" placeholder="gnt.v37@gmail.com"
														class="__txa" name="email" id="iEmail" required="required" />
												</div>
											</div>

										</div>
									</div>
									<div class="__col __pad-l-48 l8">
										<div class="">
											<div class="__tconv __pad-b-12">
												<strong>Đề xuất thuyết phục khách hàng</strong>
											</div>
											<ul>
												<li><span>1. Bạn có kinh nghiệm và kỹ năng nào
														phù hơn với dự án này?</span>
													<div class="__pad-v-16">
														<textarea
															placeholder="Tôi có kinh nghiệm trong lĩnh vực..."
															class="__txa" name="convince" id="iConvince"></textarea>
													</div></li>
												<li><span>2. Bạn dự định thực hiện dự án này như
														thế nào?</span>
													<div class="__pad-v-16">
														<textarea placeholder="Đầu tiên tôi sẽ..." class="__txa"
															name="description" id="iDescription"></textarea>
													</div></li>
											</ul>
										</div>
										<div class="__right-align">
											<button class="__bbut">Nạp đơn</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>`;
	}
}
	
class Job {
	constructor () {
		var ujOptions = document.getElementById("UserJobOptions"),
			ujEdit = document.getElementById("UserJobEdit"),
			ujForm = document.getElementById("EditForm"),
			ujApplied = document.getElementById("AppiedUserList"),
			ujContainer = document.getElementById("Container");
		console.log(ujOptions)
		if (ujOptions) 
		{

			ujOptions.addEventListener("click", function () {
				
				$(this.nextElementSibling).slideToggle("fast");
			})
		}
		
		$(ujContainer).on("click", "#CloseEditForm", function () {
			var ParentForm = this.parentElement;
			
			
			while(ParentForm.getAttribute("id") != "EditForm") {
				
				ParentForm = ParentForm.parentElement;
			}
			
			ParentForm.remove(this);
			
			ujApplied.querySelector(".__hide").classList.remove("__hide");
		})
		if (ujEdit) 
		{
			ujEdit.addEventListener("click", function () {
				
				var Node = document.createElement("div"),
					EditForm = document.getElementById("EditForm");
				
				this.parentElement.parentElement.style.display = "none";
				
				if (EditForm == null) {
					Node.innerHTML = new EditJob().getHTML();
					var Parent = this.parentElement;
					while(Parent.nodeName != "SECTION") {
						Parent = Parent.parentElement;
					}
					
					
					Parent.classList.add("__hide");
					console.log(Parent.querySelector(".Expire").innerHTML)
					
					Node.querySelector("#iUserAppliedID").value = Parent.querySelector(".AppliedUser").value;
					
					Node.querySelector("#iJobID").value = Parent.querySelector(".Job").value;
					
					Node.querySelector("#iMoney").value = parseInt(Parent.querySelector(".Money").value);
					
					Node.querySelector("#iExpire").value = parseInt(Parent.querySelector(".Expire").value);

					Node.querySelector("#iConvince").value = Parent.querySelector(".Convince").value || "";

					Node.querySelector("#iDescription").value = Parent.querySelector(".Description").value || "";

					Node.querySelector("#iPhone").value = parseInt(Parent.querySelector(".Phone").value);

					Node.querySelector("#iFacebook").value = Parent.querySelector(".Facebook").value;

					Node.querySelector("#iEmail").value = Parent.querySelector(".Email").value;
					
					
					Container.insertBefore(Node.firstElementChild, Container.lastElementChild);
				}
				
			})

		}
				
	}
}

new Job();

})();
