/**
 * 
 */
(function () {
	
class Recruitment 
{
	constructor () 
	{
		var AbilityOptions = document.getElementById("AbilityOptions"),
			ShowAbility = document.getElementById("ShowAblities"),
			Abilities = document.getElementById("Abilities");
		
		AbilityOptions.addEventListener("change", function () {
			var Node = document.createElement("a"),
				Text = this.options[this.selectedIndex].text,
				Trim = Text.replace(/-/g, "").trim();
			if (Abilities.value == "") {
				Abilities.value = this.value; 
			}
			else {
				Abilities.value += ", " + this.value; 
			}
			Node.innerHTML = Trim;
			Node.classList.add("__tag");
			ShowAbility.appendChild(Node);
		})
		autosize(document.querySelectorAll('textarea'));
	}
}

new Recruitment();
	
})();