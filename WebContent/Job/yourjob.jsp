<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="Assets/Css/library.css">
<link rel="stylesheet" href="Assets/Css/font-awesome.min.css">
<link rel="stylesheet" href="Assets/Css/default.css">
<link rel="stylesheet" href="Job/Css/yourjob.css">
<title>Your Job</title>
</head>
<body>
	<jsp:include page="/Assets/Templates/header.jsp"></jsp:include>
	<main class="__pad-t-32">
	<div class="__cwid">
		<div>
			<div class="__mar-b-32">
				<div class="__white"
					style="border: 1px solid #efefef; border-top: 0;">
					<div style="padding-top: 4px;"></div>
					<ul class="__pad-h-12 __hei42">
						<li class="__il-b __hopt __ac"><div>
								<a class="__hbut __pad-v-8 __ft-seui" href="Jobs.do"> Tuyển
									dụng </a>
							</div></li>
						<li class="__il-b __hopt"><div>
								<a class="__hbut __pad-v-8 __ft-seui" href="ListAppliedJob.do">
									Đơn xin việc </a>
							</div></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="__row">
			<div class="__col l7">
				<div>
					<ul class="__white __bor">
						<logic:iterate id="job" name="fUserJob" property="jobs">
							<li class="__bpad"><bean:define id="user" name="job"
									property="user"></bean:define> <bean:define id="id" name="user"
									property="id"></bean:define> <bean:define id="jobID" name="job"
									property="id"></bean:define>
								<div>
									<div class="__row">
										<div class="__col l8">
											<div class="__il-b">
												<h4 class="__pad-l-16">
													<a href="JobDetail.do?Job=${ jobID }" class="__color">
														<bean:write name="job" property="name" />
													</a>
												</h4>
											</div>
											<div class="__il-b __ps">
												<logic:equal value="0" name="job" property="isPublish">
													<strong><span class="__usname __f135">[
															Công việc đã được triển khai ]</span></strong>
												</logic:equal>
											</div>

											</a>
										</div>
										<div class="__col l4 __hei27">
											<div class="__tright">
												<div class="__il-b">
													<div class="__tright __lhei27">
														<time class="timeago"
															datetime="<bean:write name="job" property="created" format="MM-dd-yyy HH:mm:ss" />">
														<bean:write name="job" property="created"
															format="MM-dd-yyy hh:mm:ss" /></time>
													</div>
												</div>
												<div class="__il-b __rlt">
													<button class="__but __boptions">
														<i class="fa fa-ellipsis-v" aria-hidden="true"></i>
													</button>
													<div class="__abs __options">
														<ul class="__card-2 __white __wid186">
															<li class="__center"><a
																href="DeleteJob.do?jobID=${ jobID }" class="__but">Xóa
																	bài</a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="__mar-b-10">
										<div class="__pad-t-8">
											<div class="__row __bkcolor __pad-v-8">
												<div class="__left __pad-l-16 ">
													<span>Giá tiền</span> <strong><bean:write
															name="job" property="money" format="" /></strong>
												</div>
												<div class="__right">
													<div class="__pad-r-16">
														<span><bean:write name="job"
																property="appliedNumber" format="" /></span><span> đơn
															xin việc</span>
													</div>
												</div>
											</div>

										</div>

									</div>
									<div class="__pad-l-16">
										<ul>
											<logic:iterate id="ability" name="job"
												property="jobRequirement">
												<li class="__il-b"><span class="__tag"><bean:write
															name="ability" property="name" /></span></li>
											</logic:iterate>
										</ul>
									</div>
								</div></li>
						</logic:iterate>

					</ul>
				</div>
			</div>
			<div class="__col l5"></div>
		</div>

	</div>
	</main>
	<script type="text/javascript" src="Assets/Js/jquery-3.2.1.js"></script>
	<script type="text/javascript" src="Job/Js/yourjob.js"></script>
	<script src="Home/Js/jquery.timeago.js" type="text/javascript"></script>
	<script src="Home/Js/jquery.timeago.vi.js" type="text/javascript"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery(document).ready(function() {
				$("time.timeago").timeago();
			});

		});
	</script>
	<script src="Assets/Js/default.js"></script>
</body>
</html>