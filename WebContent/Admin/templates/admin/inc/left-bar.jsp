<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<div class="left side-menu">
	<div class="slimScrollDiv"
		style="position: relative; overflow: hidden; width: auto;">
		<div class="sidebar-inner slimscrollleft"
			style="overflow: hidden; width: auto;">

			<!-- User -->
			<div class="user-box">

				<div class="user-img">
					<logic:empty name="User" property="avatar">
						<img alt="" src="Assets/Img/anonymous.png" class="__avatar" />
						<img src="User/Img/anonymous.png" alt="user-img"
						title="Mat Helme" class="img-circle img-thumbnail img-responsive">
					</logic:empty>
					<logic:notEmpty name="User" property="avatar">
						<bean:define id="userAvatar" name="User" property="avatar"></bean:define>
						
						<img src="User/Img/${ userAvatar }" alt="user-img"
						title="Mat Helme" class="img-circle img-thumbnail img-responsive">
					</logic:notEmpty>
					
					
					<div class="user-status offline">
						<i class="zmdi zmdi-dot-circle"></i>
					</div>
				</div>
				<h5>
					<bean:define id="fn" name="User" property="firstName"></bean:define>
					<bean:define id="ln" name="User" property="lastName"></bean:define>
					<a href="#"> ${fn } ${ln } </a>
				</h5>
				<ul class="list-inline">
					<li><a href="#"> <i class="zmdi zmdi-settings"></i>
					</a></li>

					<li><a
						href="<%=request.getContextPath()%>/Logout.do"
						class="text-custom"> <i class="zmdi zmdi-power"></i>
					</a></li>
				</ul>
			</div>
			<!-- End User -->

			<!--- Sidemenu -->
			<div id="sidebar-menu">
				<ul>
					<li class="text-muted menu-title">Navigation</li>
					<li><a
						href="<%=request.getContextPath()%>/Dashboard.do"
						class="waves-effect active"><i
							class="zmdi zmdi-view-dashboard"></i> <span>Bảng điều khiển</span> </a></li>
					<li><a
						href="<%=request.getContextPath()%>/HomePage.do"
						class="waves-effect"><i
							class="zmdi zmdi-collection-item"></i> <span>Trang chủ</span> </a></li>
					
					<li class="has_sub"><a
						href="<%=request.getContextPath()%>/UserList.do"
						class="waves-effect"><i class="ti-user"></i> <span>
								Quản lý user </span></a></li>
					<li class="has_sub"><a href="javascript:void(0);"
						class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span>
								Quản lý bài đăng </span> <span class="menu-arrow"></span></a>
						<ul class="list-unstyled">
							<li><a href="ui-buttons.html">Nhà tuyển dụng</a></li>
							<li><a href="ui-cards.html">Người tìm việc</a></li>
						</ul></li>
					<li class="has_sub"><a
						href="<%=request.getContextPath()%>/admin/quan-ly-nghanh.do"
						class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span>
								Quản lý nghành nghề </span></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<!-- Sidebar -->
			<div class="clearfix"></div>

		</div>
		<div class="slimScrollBar"
			style="background: rgb(220, 220, 220); width: 5px; position: absolute; top: 165px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 334.23px; visibility: visible;"></div>
		<div class="slimScrollRail"
			style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
	</div>

</div>