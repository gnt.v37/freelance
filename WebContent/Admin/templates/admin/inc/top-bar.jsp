<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="<%=request.getContextPath() %>/Admin/templates/admin/assets/images/favicon.ico">

        <title>Quangto - Dashboard</title>

        <!--Morris Chart CSS -->
		<link rel="stylesheet" href="<%=request.getContextPath() %>/Admin/templates/admin/assets/plugins/morris/morris.css">

        <!-- App css -->
        <link href="<%=request.getContextPath() %>/Admin/templates/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath() %>/Admin/templates/admin/assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath() %>/Admin/templates/admin/assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath() %>/Admin/templates/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath() %>/Admin/templates/admin/assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath() %>/Admin/templates/admin/assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath() %>/Admin/templates/admin/assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/js/modernizr.min.js"></script>

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.html" class="logo"><span>Quảng<span>to</span></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Trang quản trị</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->

    