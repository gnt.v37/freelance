<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="side-bar right-bar"><a href="javascript:void(0);"
	class="right-bar-toggle"> <i class="zmdi zmdi-close-circle-o"></i>
</a>
<h4 class="">Notifications</h4>
<div class="notification-list nicescroll" tabindex="5001"
	style="overflow: hidden; outline: none;">
<ul class="list-group list-no-border user-list">
	<li class="list-group-item"><a href="#" class="user-list-item">
	<div class="avatar"><img src="<%=request.getContextPath() %>/templates/admin/assets/images/users/avatar-2.jpg"
		alt=""></div>
	<div class="user-desc"><span class="name">Michael Zenaty</span> <span
		class="desc">There are new settings available</span> <span
		class="time">2 hours ago</span></div>
	</a></li>
	<li class="list-group-item"><a href="#" class="user-list-item">
	<div class="icon bg-info"><i class="zmdi zmdi-account"></i></div>
	<div class="user-desc"><span class="name">New Signup</span> <span
		class="desc">There are new settings available</span> <span
		class="time">5 hours ago</span></div>
	</a></li>
	<li class="list-group-item"><a href="#" class="user-list-item">
	<div class="icon bg-pink"><i class="zmdi zmdi-comment"></i></div>
	<div class="user-desc"><span class="name">New Message
	received</span> <span class="desc">There are new settings available</span> <span
		class="time">1 day ago</span></div>
	</a></li>
	<li class="list-group-item active"><a href="#"
		class="user-list-item">
	<div class="avatar"><img src="<%=request.getContextPath() %>/templates/admin/assets/images/users/avatar-3.jpg"
		alt=""></div>
	<div class="user-desc"><span class="name">James Anderson</span> <span
		class="desc">There are new settings available</span> <span
		class="time">2 days ago</span></div>
	</a></li>
	<li class="list-group-item active"><a href="#"
		class="user-list-item">
	<div class="icon bg-warning"><i class="zmdi zmdi-settings"></i></div>
	<div class="user-desc"><span class="name">Settings</span> <span
		class="desc">There are new settings available</span> <span
		class="time">1 day ago</span></div>
	</a></li>

</ul>
</div>
<div id="ascrail2001" class="nicescroll-rails"
	style="width: 8px; z-index: 999; cursor: default; position: absolute; top: 57px; left: 232px; height: 918px; display: none;">
<div
	style="position: relative; top: 0px; float: right; width: 6px; height: 0px; background-color: rgb(152, 166, 173); border: 1px solid rgb(255, 255, 255); background-clip: padding-box; border-radius: 5px;"></div>
</div>
<div id="ascrail2001-hr" class="nicescroll-rails"
	style="height: 8px; z-index: 999; top: 967px; left: 0px; position: absolute; cursor: default; display: none;">
<div
	style="position: relative; top: 0px; height: 6px; width: 0px; background-color: rgb(152, 166, 173); border: 1px solid rgb(255, 255, 255); background-clip: padding-box; border-radius: 5px;"></div>
</div>
</div>