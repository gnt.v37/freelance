<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 </div>
        <!-- END wrapper -->
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/js/jquery.min.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/js/bootstrap.min.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/js/detect.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/js/fastclick.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/js/jquery.slimscroll.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/js/jquery.blockUI.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/js/waves.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/js/jquery.nicescroll.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/js/jquery.scrollTo.min.js"></script>

        <!-- Datatables-->
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/plugins/datatables/jszip.min.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/plugins/datatables/dataTables.scroller.min.js"></script>

        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/pages/datatables.init.js"></script>

        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/js/jquery.core.js"></script>
        <script src="<%=request.getContextPath() %>/Admin/templates/admin/assets/js/jquery.app.js"></script>
    </body>
</html>