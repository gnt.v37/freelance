<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/Admin/templates/admin/inc/top-bar.jsp"%>
<%@include file="/Admin/templates/admin/inc/left-bar.jsp"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<div class="content-page">
	<div class="content">
		<div class="container">


			<div class="row">

				<div class="col-lg-6 col-md-6">
					<div class="card-box">

						<h4 class="header-title m-t-0 m-b-30">Tổng số bài đăng trong
							tuần</h4>

						<div class="widget-chart-1" style="background-color: azure;">
							<div class="widget-chart-box-1"></div>

							<div class="widget-detail-1"></div>
						</div>
					</div>
				</div>
				<!-- end col -->

				<div class="col-lg-6 col-md-6">
					<div class="card-box">

						<h4 class="header-title m-t-0 m-b-30">Tổng số lượt tìm việc
							trong tuần</h4>

						<div class="widget-box-2" style="background-color: azure;"></div>
					</div>
				</div>
				<!-- end col -->
				<!-- end col -->
				<!-- end col -->
			</div>
			<!-- end row -->




			<div class="row">
				<div class="col-lg-12">
					<div class="card-box">

						<h4 class="header-title m-t-0 m-b-30">Việc làm mới nhất</h4>
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>Id</th>
										<th>User</th>
										<th>Category</th>
										<th>Address</th>
										<th>JobName</th>
										<th>Money</th>
										<th>Description</th>
										<th>ApplicationExpire</th>
										<th>AppliedNumber</th>
										<th>Create</th>
									</tr>
								</thead>
								<tbody>
									<logic:iterate id="job" name="fDashboard" property="listJob">
										<bean:define id="user" name="job" property="user"></bean:define>
										<bean:define id="category" name="job" property="category"></bean:define>
										<bean:define id="address" name="job" property="address"></bean:define>
										<tr>
											<td scope="row"><bean:write name="job" property="id"
													format="" /></td>
											<td scope="row"><bean:write name="user"
													property="firstName" /> <bean:write name="user"
													property="lastName" /></td>
											<td scope="row"><bean:write name="category"
													property="name" /></td>
											<td scope="row"><bean:write name="address"
													property="name" /></td>
											<td scope="row"><bean:write name="job" property="name" />
											</td>
											<td scope="row"><bean:write name="job" property="money"
													format="" /></td>
											<td scope="row"><bean:write name="job"
													property="description" /></td>
											<td scope="row"><bean:write name="job"
													property="applicationExpire" format="MM-dd-yyyy" /></td>
											<td scope="row"><bean:write name="job"
													property="appliedNumber" format="" /></td>
											<td scope="row"><bean:write name="job"
													property="created" format="DD-mm-yyyy" /></td>
										</tr>

									</logic:iterate>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- end col -->
			</div>
			<!-- end row -->
		</div>
		<!-- container -->
	</div>
	<!-- content -->
	<footer class="footer text-right"> 2017 © Adminto. </footer>
</div>
</div>
<%@include file="/Admin/templates/admin/inc/footer.jsp"%>