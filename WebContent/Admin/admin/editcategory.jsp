<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@include file="/Admin/templates/admin/inc/top-bar.jsp"%>
<%@include file="/Admin/templates/admin/inc/left-bar.jsp"%>

<%@page import="java.util.ArrayList"%>
<%@page import="models.bean.Address"%>
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">

			<div class="row">
				<div class="col-lg-12">
					<div class="card-box">

						<h4 class="header-title m-t-0 m-b-30">Sửa nghành</h4>
						<bean:define id="cate" name="CategoryForm" property="category"></bean:define>
						<bean:define id="id" name="cate" property="categoryId"></bean:define>

						<form
							action="<%=request.getContextPath() %>/admin/edit-category.do?id=${id}"
							method="post">
							<bean:define id="name" name="cate" property="categoryName"></bean:define>
							<div class="form-group">
								<label for="userName">Category Name*</label> <input type="text"
									name="categoryname" parsley-trigger="change" required=""
									value="${name }" placeholder="Enter category name"
									class="form-control" id="categoryname">
							</div>
							<div class="form-group text-right m-b-0">
								<html:submit
									styleClass="btn btn-primary waves-effect waves-light"
									value="submit" property="submit">submit</html:submit>
								<button type="reset"
									class="btn btn-default waves-effect waves-light m-l-5">
									Hủy</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end col -->
			</div>
		</div>
		<!-- container -->
	</div>
	<!-- content -->

	<footer class="footer"> 2016 © Adminto. </footer>

</div>
<script>
	var resizefunc = [];
</script>

<!-- jQuery  -->
<script
	src="<%=request.getContextPath()%>/Admin/admin/assets/js/jquery.min.js"></script>
<script
	src="<%=request.getContextPath()%>/Admin/admin/assets/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/admin/assets/js/detect.js"></script>
<script
	src="<%=request.getContextPath()%>/Admin/admin/assets/js/fastclick.js"></script>
<script
	src="<%=request.getContextPath()%>/Admin/admin/assets/js/jquery.slimscroll.js"></script>
<script
	src="<%=request.getContextPath()%>/Admin/admin/assets/js/jquery.blockUI.js"></script>
<script src="<%=request.getContextPath()%>/admin/assets/js/waves.js"></script>
<script
	src="<%=request.getContextPath()%>/Admin/admin/assets/js/jquery.nicescroll.js"></script>
<script
	src="<%=request.getContextPath()%>/Admin/admin/assets/js/jquery.scrollTo.min.js"></script>

<!-- Validation js (Parsleyjs) -->
<script type="text/javascript"
	src="<%=request.getContextPath()%>/Admin/admin/assets/plugins/parsleyjs/dist/parsley.min.js"></script>

<!-- App js -->
<script
	src="${pageContext.request.contextPath }/resources/assets/js/jquery.core.js"></script>
<script
	src="${pageContext.request.contextPath }/resources/assets/js/jquery.app.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('form').parsley();
	});
</script>
<%@include file="/Admin/templates/admin/inc/side-bar right-bar.jsp"%>
<%@include file="/Admin/templates/admin/inc/footer.jsp"%>