<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/Admin/templates/admin/inc/top-bar.jsp"%>
<%@include file="/Admin/templates/admin/inc/left-bar.jsp"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="card-box table-responsive">

						<h4 class="header-title m-t-0 m-b-30">Trang quản lý user</h4>
						<span
							class="btn btn-inverse btn-trans waves-effect w-md waves-inverse m-b-5"><a
							href="<%=request.getContextPath()%>/AddUser.do">Thêm
								user</a></span>
						<table id="datatable" class="table table-striped table-bordered">
							<thead>
								<tr style="text-align: center;">
									<th>Id</th>
									<th>Avatar</th>
									<th>Full Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>Address</th>
									<th>DateUpdate</th>
									<th>Function</th>
								</tr>
							</thead>
							<tbody>
								<logic:iterate id="user" name="fUserForm" property="listUser">
									<bean:define id="address" name="user" property="address"></bean:define>
									<bean:define id="em" name="user" property="email"></bean:define>
									<tr>
										<td scope="row"><bean:write name="user" property="id"
												format="" /></td>
										<td scope="row"><logic:empty name="user"
												property="avatar">
											
												<img height="72px"
													src="<%=request.getContextPath()%>/Assets/Img/anonymous.png" />
												
											</logic:empty> <logic:notEmpty name="user" property="avatar">
												<bean:define id="userAvatar" name="user" property="avatar"></bean:define>

												<img height="72px"
													src="<%=request.getContextPath()%>/User/Img/${ userAvatar }"></img>
											</logic:notEmpty></td>
										<td scope="row"><bean:write name="user"
												property="firstName" /> <bean:write name="user"
												property="lastName" /></td>
										<td scope="row">${em }</td>
										<td scope="row"><bean:write name="user" property="phone" />
										</td>
										<td scope="row"><bean:write name="address"
												property="name" /></td>
										<td scope="row"><bean:write name="user"
												property="created" format="MM-dd-yyyy" /></td>
										<logic:equal value="ducquangcit@gmail.com" name="User"
											property="email">
											<td scope="row"><bean:define id="id" name="user"
													property="id"></bean:define> <a
												href="<%=request.getContextPath() %>/admin/edit.do?id=${id}"
												style="float: left;">Sửa<span class="fa fa-pencil"></span>
											</a> <a
												href="<%=request.getContextPath() %>/admin/del.do?id=${id}"
												style="float: right;">Xóa<span class="fa fa-trash-o"></span></a>
											</td>
										</logic:equal>
										<logic:equal value="${em}" name="User" property="email">
											<td scope="row"><bean:define id="id" name="user"
													property="id"></bean:define> <a
												href="<%=request.getContextPath() %>/admin/edit.do?id=${id}"
												style="float: left;">Sửa<span class="fa fa-pencil"></span>
											</a></td>
										</logic:equal>
										<logic:notEqual value="${em}" name="User" property="email">
											<td scope="row"></td>
										</logic:notEqual>
									</tr>
								</logic:iterate>
							</tbody>
						</table>
					</div>
				</div>
				<!-- end col -->
			</div>


		</div>
		<!-- container -->

	</div>
	<!-- content -->

	<footer class="footer"> 2016 © Adminto. </footer>

</div>
<!-- Right Sidebar -->
<%@include file="/Admin/templates/admin/inc/side-bar right-bar.jsp"%>
<%@include file="/Admin/templates/admin/inc/footer.jsp"%>
