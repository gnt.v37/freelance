<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App title -->
        <title>Adminto</title>

        <!-- App CSS -->
        <link href="<%=request.getContextPath() %>/templates/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath() %>/templates/admin/assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath() %>/templates/admin/assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath() %>/templates/admin/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath() %>/templates/admin/assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath() %>/templates/admin/assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath() %>/templates/admin/assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<%=request.getContextPath() %>/templates/admin/assets/js/modernizr.min.js"></script>
        
    </head>
    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="text-center">
                <a href="index.html" class="logo"><span>Admin<span>to</span></span></a>
                <h5 class="text-muted m-t-0 font-600">Responsive Admin Dashboard</h5>
            </div>
        	<div class="m-t-40 card-box">
                <div class="text-center">
                    <h4 class="text-uppercase font-bold m-b-0">Đăng nhập hệ thống</h4>
                </div>
                <div class="panel-body">
                    <html:form action="/admin/dang-nhap.do" method="post" styleClass="form-horizontal m-t-20"  >

                        <div class="form-group ">
                            <div class="col-xs-12">
                            	<label>Email(*)</label>
                            	<html:text property="email" value="" styleClass="form-control"  ></html:text>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                            	<label>Password(*)</label>
                            	<html:password property="passWord" styleClass="form-control" ></html:password>
                            </div>
                        </div>


                        <div class="form-group text-center m-t-30">
                            <div class="col-xs-12">
                            	<html:submit  property="submit" value="submit" styleClass="btn btn-custom btn-bordred btn-block waves-effect waves-light" ></html:submit>
                            </div>
                            
                        </div>

                        <div class="form-group m-t-30 m-b-0">
                            <div class="col-sm-12">
                                <a href="page-recoverpw.html" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                            </div>
                        </div>
                    </html:form>

                </div>
            </div>
            <!-- end card-box-->
        </div>
        <!-- end wrapper page -->
        

        
    	<script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<%=request.getContextPath() %>/templates/admin/assets/js/jquery.min.js"></script>
        <script src="<%=request.getContextPath() %>/templates/admin/assets/js/bootstrap.min.js"></script>
        <script src="<%=request.getContextPath() %>/templates/admin/assets/js/detect.js"></script>
        <script src="<%=request.getContextPath() %>/templates/admin/assets/js/fastclick.js"></script>
        <script src="<%=request.getContextPath() %>/templates/admin/assets/js/jquery.slimscroll.js"></script>
        <script src="<%=request.getContextPath() %>/templates/admin/assets/js/jquery.blockUI.js"></script>
        <script src="<%=request.getContextPath() %>/templates/admin/assets/js/waves.js"></script>
        <script src="<%=request.getContextPath() %>/templates/admin/assets/js/wow.min.js"></script>
        <script src="<%=request.getContextPath() %>/templates/admin/assets/js/jquery.nicescroll.js"></script>
        <script src="<%=request.getContextPath() %>/templates/admin/assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="<%=request.getContextPath() %>/templates/admin/assets/js/jquery.core.js"></script>
        <script src="<%=request.getContextPath() %>/templates/admin/assets/js/jquery.app.js"></script>
	
	</body>
</html>