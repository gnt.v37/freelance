<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/Admin/templates/admin/inc/top-bar.jsp"%>
<%@include file="/Admin/templates/admin/inc/left-bar.jsp"%>
<%@page import="java.util.ArrayList"%>
<%@page import="models.bean.User"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="card-box table-responsive">

						<h4 class="header-title m-t-0 m-b-30">Trang quản lý nghành
							nghề</h4>
						<span
							class="btn btn-inverse btn-trans waves-effect w-md waves-inverse m-b-5"><a
							href="<%=request.getContextPath()%>/admin/them-nghanh.do">Thêm
								nghành</a></span>
						<table id="datatable" class="table table-striped table-bordered">
							<thead>
								<tr style="text-align: center;">
									<th>Id</th>
									<th>Category Name</th>
									<th>Function</th>
								</tr>
							</thead>
							<tbody>
								<logic:iterate id="category" name="CategoryForm"
									property="listCategories">

									<tr>
										<td scope="row"><bean:write name="category"
												property="categoryId" /></td>
										<td scope="row"><bean:write name="category"
												property="categoryName" /></td>
										<td scope="row"><bean:define id="id" name="category"
												property="categoryId"></bean:define> <a
											href="<%=request.getContextPath() %>/Admin/admin/edit-category.do?id=${id}"
											style="float: left;">Sửa<span class="fa fa-pencil"></span>
										</a> <a
											href="<%=request.getContextPath() %>/Admin/admin/del-category.do?id=${id}"
											style="float: right;">Xóa<span class="fa fa-trash-o"></span></a>
										</td>
									</tr>
								</logic:iterate>

							</tbody>
						</table>
					</div>
				</div>
				<!-- end col -->
			</div>


		</div>
		<!-- container -->

	</div>
	<!-- content -->

	<footer class="footer"> 2016 © Adminto. </footer>

</div>
<%@include file="/Admin/templates/admin/inc/side-bar right-bar.jsp"%>
<%@include file="/Admin/templates/admin/inc/footer.jsp"%>
