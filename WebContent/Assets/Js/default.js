/**

 * 
 */

class $Get {
    constructor(Url)
    {
        return new Promise(function (Resolve, Reject)
        {
            var Request = new XMLHttpRequest();

            Request.open("GET", Url);

            Request.onload = function ()
            {
                if (Request.status == 200)
                {
                    Resolve(this.response);
                }
                else
                {
                    Reject(Error(this.statusText))
                }
            }

            Request.onerror = function ()
            {
                Reject(Error("Network Error"));
            }

            Request.send();
        })
    }
}

class $Post
{
    constructor(Url, Data)
    {
        var Cookie = this.Cookie();
        return new Promise(function (Resolve, Reject)
        {
            var Request = new XMLHttpRequest();

            Request.open("POST", Url);

            Request.onload = function ()
            {
                if (Request.status == 200)
                {
                    Resolve(this.response);
                }
                else
                {
                    Reject(Error(this.statusText))
                }
            }

            Request.onerror = function ()
            {
                Reject(Error("Network Error"));
            }

            Request.send(Data);
        })
    }

    Cookie(Name = "csrftoken")
    {
        var Value = null;
        if (document.cookie && document.cookie !== '')
        {
            var Cookies = document.cookie.split(';');
            for (var i = 0; i < Cookies.length; i++)
            {
                var Cookie = jQuery.trim(Cookies[i]);
                // Does this cookie string begin with the name we want?
                if (Cookie.substring(0, Name.length + 1) === (Name + '='))
                {
                    Value = decodeURIComponent(Cookie.substring(Name.length + 1));
                    break;
                }
            }
        }
        return Value;
    }
}

class $Events
{
	constructor ()
	{
		this.Notification = document.getElementById("Noti");
		this.User = document.getElementById("UsOp");
		this.Search 	  = document.getElementById("Search");
		this.PathName 	  = window.location.pathname.split("/");
		console.log(this.PathName[1])
		this.Click ();
	}
	
	Click ()
	{
		
			
		var Toggle = function (Data)
		{
			var IsBlock  = Data.querySelector(".__conta.__block"),
				ThisCout = Data.querySelector(".__conta");
			if (IsBlock)
			{
				ThisCout.classList.remove("__block");
			}
			else
			{
				ThisCout.classList.add("__block");
			}
			var Cont = Data.parentElement.querySelectorAll(".__conta.__block");
			
			Cont.forEach (function (Element) {
				
				if (!Element.isEqualNode(ThisCout))
				{
					Element.classList.remove("__bl");
				}
			})
			
			
		}
		
		this.User.addEventListener ("click", function () {
			console.log(this)
			Toggle(this);
		})
		
		var _this = this;
		this.Notification.addEventListener("click", function () {
			Toggle(this);
			var UserID = this.getAttribute("data"),
				Notifi = this.querySelector("#noticationNumber");
			console.log(_this)
			_this.Notifi(Notifi, UserID);
		})
		

		
		document.body.addEventListener("click", function (e) {
			var ExistBlock = document.querySelector(".__conta.__block");
			if (ExistBlock)
			{
				var ParentID = ExistBlock.parentElement.getAttribute("id");
				if (!e.target.closest(`#${ParentID}`))
				{
					ExistBlock.classList.remove("__block");
				}
			}
		})
		
	}
	
	
	Notifi (Element, UserID) {
		
		var Data = new FormData(),
			Noti = document.getElementById("ListNotification");
		
		Data.append("userID", UserID);
		
		
	
		new $Post("ReadNotification.do", Data).then (function (Message) {
			
			var Node = document.createElement("div");
			
			Node.innerHTML = Message;
			
			console.log(Node)
			
			if (Noti.firstElementChild == null || Noti.firstElementChild.length <= 0) {
				Noti.appendChild(Node.firstElementChild);
				
				$(Noti.querySelectorAll("time")).timeago()
			}
			
			
			Element.parentElement.removeChild(Element);
		}, function (Error) {
			console.log(Error)
		});
		
	}
	
}
new $Events();