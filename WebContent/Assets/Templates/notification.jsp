<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<logic:iterate id="notification" name="fNotification"
	property="notifications">
	<bean:define id="jobID" name="notification" property="jobID"></bean:define>
	<bean:define id="message" name="notification" property="notification"></bean:define>
	<li class="__bkg __bor" notification="95" seen="1"><a
		href="JobDetail.do?Job=${ jobID }" class="__pad-12">
			<div class="__row">

				<div>
					<bean:write name="notification" property="notification" />
				</div>
				<div class="__time">
					<time datetime="<bean:write name="notification" property="notificationTime"
						format="MM-dd-yyyy HH:mm:ss" />"></time>
				</div>

			</div>
	</a></li>
</logic:iterate>