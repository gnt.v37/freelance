<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<header class="">
	<div class="__head __white __card-2">
		<div id="FixedHeader" class="__pad-h-64">
			<ul class="__row">
				<li class="__left" style="padding-top: 8px;"><a href="">
						<div class="__row">
							<div class="__left">
								<img src="/geye/assets/static/images/icon.png">
							</div>
							<div class="__left __item-center" style="height: 47px;">
								<strong style="font-size: 20px;"><span class="__color">G</span>E<span
									class="__color">Y</span>E</strong>
							</div>
						</div>
				</a></li>
				<li><logic:empty name="User">
						<li class="__right __flex-cc __mar-8" style="min-height: 46px;"><a
							id="UserLoading" class="__color" href="Login.do">Đăng nhập /
								Đăng ký</a></li>
					</logic:empty> <logic:notEmpty name="User">

						<bean:define id="userID" name="User" property="id"></bean:define>

						<li class="__right __flex-cc __mar-8" style="min-height: 46px;">
							<div>
								<ul class="__row">
									<li class="__left" id="UsOp"><a id="UserLoading"
										class="__color ">
											<div class="__row">
												<div class="__left">
													<logic:empty name="User" property="avatar">
														<img alt="" src="Assets/Img/anonymous.png"
															class="__avatar" />
													</logic:empty>
													<logic:notEmpty name="User" property="avatar">
														<bean:define id="userAvatar" name="User" property="avatar"></bean:define>
														<img alt="" src="User/Img/${ userAvatar }"
															class="__avatar" />
													</logic:notEmpty>

												</div>
												<div class="__left __pad-l-12 __uinfo">
													<bean:write name="User" property="firstName" />
													<bean:write name="User" property="lastName" />
												</div>
											</div>
									</a>
										<div id="UserOptions" class="__conta">

											<div class='__crt'></div>
											<div class='__bak'>
												<div class='__crt'></div>
												<div class='__pad-12'></div>

												<ul id='ListUserOption'>
													<li class="__bkg __bor"><a
														href="Profile.do?User=${ userID }" class="__pad1012">Trang
															cá nhân</a></li>
													<logic:equal value="1" name="User" property="isAdmin">
														<li class="__bkg __bor"><a href="Dashboard.do"
															class="__pad1012">Quản trị</a></li>
													</logic:equal>

													<li class="__bkg __bor"><a href="Logout.do"
														class="__pad1012">Đăng xuất</a></li>
												</ul>
											</div>
										</div></li>
									<li class="__pos3 __ft-seui __left" id="Noti"
										data="${ userID }"><bean:include id="noti"
											forward="NotificationNumber" /> <bean:write name="noti"
											filter="false" />

										<div id="Notifications" class="__conta">

											<div class='__crt'></div>
											<div class='__bak'>
												<div class='__crt'></div>
												<div class='__pad-12'>Thông báo</div>
												<div>
													<ul id='ListNotification'>

													</ul>
												</div>
											</div>
										</div></li>
								</ul>
							</div>
						</li>
					</logic:notEmpty></li>

			</ul>
		</div>
		<div>
			<div class="__bdt __pad-h-64">
				<ul class="__row" style="padding-top: 4px;">
					<li class="__col l4 m4">
						<ul class="__row">
							<li class="__left __hopt"><div>
									<a class="__hbut __pad-v-8 __ft-seui" href="HomePage.do">Trang
										Chủ</a>
								</div></li>
							<logic:notEmpty name="User">

								<bean:define id="userID" name="User" property="id"></bean:define>
								<li class="__left __hopt"><div>
										<a class="__hbut __pad-v-8 __ft-seui"
											href="Jobs.do?userID=${ userID }">Góc công việc</a>
									</div></li>
							</logic:notEmpty>
						</ul>
					</li>
					<li class="__col l4 m4">
						<div>
							<form action="Search.do" method="POST">
								<input type="search" placeholder="Tìm kiếm công việc"
									class="__input __ft-seui" name="all"
									style="border-bottom: none;">
							</form>
						</div>
					</li>
					<li class="__col l4 m4">
						<ul class="__row __right">
							<logic:notEmpty name="User">
								<li class="__left __hopt"><div style="padding-right: 0;">
										<a class="__hbut __pad-v-8 __ft-seui" href="Recruitment.do">Đăng
											công việc</a>
									</div></li>
							</logic:notEmpty>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="__head-wid"></div>
</header>
