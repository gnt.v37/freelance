<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<button class="__nobut">
	<i class="fa fa-bell" aria-hidden="true"></i>
	<logic:notEqual value="0" name="fNotification"
		property="notificationNumber">
		<span id="noticationNumber"> <bean:write name="fNotification"
				property="notificationNumber" format="" />
		</span>
	</logic:notEqual>
</button>

