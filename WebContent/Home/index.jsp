<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="Assets/Css/library.css">

<link rel="stylesheet" href="Assets/Css/font-awesome.min.css">
<link rel="stylesheet" href="Assets/Css/default.css">
<link rel="stylesheet" href="Home/Css/home.css">
<title>Home Page</title>

</head>
<body>
	<jsp:include page="/Assets/Templates/header.jsp"></jsp:include>
	<main class="__pad-t-32">
	<div class="__cwid">
		<div class="__row">
			<div class="__col l3">
				<div class="__white __pad-v-12 __bor">
					<div class="__pad-b-12 __borbot">
						<h5 class="__pad-h-16">
							<strong>Ngành Nghề</strong>
						</h5>
					</div>
					<div class="">
						<ul class="__career __pad-t-8">
							<li class=""><a href="Search.do?categoryID=0"
								class="__color">Tất cả</a></li>
							<logic:iterate id="category" name="fHomePage"
								property="categoryList">
								<bean:define id="categoryID" name="category" property="id"></bean:define>
								<li><a href="Search.do?categoryID=${ categoryID }"
									class="__color"><bean:write name="category" property="name" /></a>
								</li>
							</logic:iterate>
						</ul>
					</div>

				</div>
				<div class="__white __mar-t-16 __pad-v-12 __bor">
					<div class="__pad-b-12 __borbot">
						<h5 class="__pad-h-16">
							<strong>Giá tiền</strong>
						</h5>
					</div>
					<div class="">
						<ul class="__career __pad-t-8 __smoney">
							<li class=""><a href="Search.do?moneyID=0" class="__color"
								style="padding-left: 18px;">Tất cả</a></li>
							<li class="">
								<div></div> <a href="Search.do?moneyID=1" class="__color"> <span
									class="__w16 __il-b">></span> <span>200.000.000 </span>
							</a>
							</li>
							<li class=""><a href="Search.do?moneyID=2" class="__color">
									<span class="__w16 __il-b">></span> <span class="__w80 __il-b">50.000.000
								</span> <span class="__w16 __il-b">< </span> <span>200.000.000 </span>
							</a></li>
							<li class=""><a href="Search.do?moneyID=3" class="__color">
									<span class="__w16 __il-b">></span> <span class="__w80 __il-b">10.000.000
								</span> <span class="__w16 __il-b">< </span> <span>50.000.000 </span>
							</a></li>
							<li class=""><a href="Search.do?moneyID=4" class="__color">
									<span class="__w16 __il-b">></span> <span class="__w80 __il-b">5.000.000
								</span> <span class="__w16 __il-b">< </span> <span>10.000.000 </span>
							</a></li>

							<li class=""><a href="Search.do?moneyID=5" class="__color">
									<span class="__w16 __il-b"><</span> <span class="__w80 __il-b">5.000.000
								</span>
							</a></li>
						</ul>
					</div>

				</div>
			</div>
			<div class="__col l9 __pad">
				<div class="__eerror">
					<bean:write name="fHomePage" property="message" />
				</div>
				<div class="__white __bor">
					<logic:iterate id="job" name="fHomePage" property="jobList">
						<bean:define id="jobUser" name="job" property="user"></bean:define>
						<bean:define id="jobCategory" name="job" property="category"></bean:define>
						<bean:define id="jobAddress" name="job" property="address"></bean:define>
						<bean:define id="jobID" name="job" property="id"></bean:define>
						<bean:define id="userID" name="jobUser" property="id"></bean:define>
						<section>
						<div>
							<ul>
								<li class="__title __mar-b-5 __pad-h-16">
									<div class="__row">
										<div class="__left">
											<h4>
												<a href="JobDetail.do?Job=${ jobID }" class="__color"> <bean:write
														name="job" property="name" />
												</a>
											</h4>
										</div>
										<div class="__right" style="padding-top: 6px;">
											<time class="timeago"
												datetime="<bean:write name="job" property="created" format="MM-dd-yyy HH:mm:ss" />">
											<bean:write name="job" property="created"
												format="MM-dd-yyy hh:mm:ss" /></time>
										</div>
									</div>
								</li>
								<li class="__pad-h-16 __pad-b-8"><a class="__usname"
									href="Profile.do?User=${ userID }"> <bean:write
											name="jobUser" property="firstName" /> <bean:write
											name="jobUser" property="lastName" />
								</a></li>
								<li class="__pad-b-8">
									<div class="__bkg __row __pad-10">
										<div class="__left">
											<span> <bean:write name="jobAddress" property="name" />
											</span> <span class="__pad-v-3">|</span> <span> <bean:write
													name="jobCategory" property="name" />
											</span> <span class="__pad-v-3">|</span> <span> <bean:write
													name="job" property="money" format=",000" /> đ
											</span>
										</div>
										<div class="__right">
											<span>Hạn nạp đơn</span>
											<bean:write name="job" property="applicationExpire"
												format="MM-dd-yyyy" />
										</div>
									</div>
								</li>
								<li class="__pad-h-16">
									<div class="__mar-b-12">
										<bean:write name="job" property="description" />
									</div>
								</li>
								<li class="__pad-h-16">
									<div class="__row">
										<div class="__left">
											<div class="">
												<div class="__il-b">
													<span class="__bcolor __tag __btag"><bean:write
															name="jobCategory" property="name" /></span>
												</div>
												<div class="__il-b">
													<ul>
														<li><logic:iterate id="requirement" name="job"
																property="jobRequirement">
																<bean:define id="reqID" name="requirement" property="id"></bean:define>
																<a class="__tag __il-b" href="Search.do?abilityID=${ reqID }"> <bean:write
																		name="requirement" property="name" />
																</a>
															</logic:iterate></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="__right">
											<span> <bean:write name="job" property="appliedNumber"
													format="" />
											</span><span>Đơn xin việc</span>
										</div>
									</div>
								</li>
							</ul>
						</div>
						</section>
					</logic:iterate>
				</div>
			</div>
		</div>
	</div>
	</main>
	<script type="text/javascript" src="Assets/Js/jquery-3.2.1.js"></script>
	<script src="Home/Js/jquery.timeago.js" type="text/javascript"></script>
	<script src="Home/Js/jquery.timeago.vi.js" type="text/javascript"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery(document).ready(function() {
				$("time.timeago").timeago();
			});

		});
	</script>
	<script src="Assets/Js/default.js"></script>
</body>
</html>