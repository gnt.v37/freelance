package forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import models.bean.Category;
import models.bean.Job;

public class FHomePage extends ActionForm {

	/** 
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ArrayList<Job> jobList;

	private Job job;
	private String categoryID, all, moneyID, abilityID;
	
	public String getAbilityID() {
		return abilityID;
	}

	public void setAbilityID(String abilityID) {
		this.abilityID = abilityID;
	}

	private ArrayList<Category> categoryList;

	private String message;

	public String getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(String categoryID) {
		this.categoryID = categoryID;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

	public ArrayList<Category> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(ArrayList<Category> categoryList) {
		this.categoryList = categoryList;
	}

	public ArrayList<Job> getJobList() {
		return jobList;
	}

	public String getMoneyID() {
		return moneyID;
	}

	public void setMoneyID(String moneyID) {
		this.moneyID = moneyID;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public void setJobList(ArrayList<Job> jobList) {
		this.jobList = jobList;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

}
