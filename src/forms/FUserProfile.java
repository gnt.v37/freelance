package forms;

import org.apache.struts.action.ActionForm;

import models.bean.User;

public class FUserProfile extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
	
}
