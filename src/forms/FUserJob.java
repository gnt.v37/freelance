package forms;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import models.bean.Job;

public class FUserJob extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int userID;
	
	private ArrayList<Job> jobs;
	
	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public ArrayList<Job> getJobs() {
		return jobs;
	}

	public void setJobs(ArrayList<Job> jobs) {
		this.jobs = jobs;
	}
	
}
