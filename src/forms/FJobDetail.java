package forms;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import models.bean.Ability;
import models.bean.Job;
import models.bean.UserJob;

public class FJobDetail extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int applied;
	
	private Job job;
	
	private int isPublish;
	
	private int appliedNumber;
	
	private ArrayList<Ability> abilities;
	
	public ArrayList<Ability> getAbilities() {
		return abilities;
	}

	public void setAbilities(ArrayList<Ability> abilities) {
		this.abilities = abilities;
	}

	public int getAppliedNumber() {
		return appliedNumber;
	}

	public void setAppliedNumber(int appliedNumber) {
		this.appliedNumber = appliedNumber;
	}
	
	public int getIsPublish() {
		return isPublish;
	}

	public void setIsPublish(int isPublish) {
		this.isPublish = isPublish;
	}

	private ArrayList<UserJob> userAppliedList;

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}
	
	public int getApplied() {
		return applied;
	}

	public void setApplied(int applied) {
		this.applied = applied;
	}

	public ArrayList<UserJob> getUserAppliedList() {
		return userAppliedList;
	}

	public void setUserAppliedList(ArrayList<UserJob> userAppliedList) {
		this.userAppliedList = userAppliedList;
	}

}
