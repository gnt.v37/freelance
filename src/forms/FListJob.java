package forms;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import models.bean.UserJob;

public class FListJob extends ActionForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ArrayList<UserJob> listJob;

	public ArrayList<UserJob> getListJob() {
		return listJob;
	}

	public void setListJob(ArrayList<UserJob> listJob) {
		this.listJob = listJob;
	}
	

}
