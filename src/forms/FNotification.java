package forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import models.bean.Notification;

public class FNotification extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ArrayList<Notification> notifications;

	private int notificationNumber;
	
	
	
	public int getNotificationNumber() {
		return notificationNumber;
	}

	public void setNotificationNumber(int notificationNumber) {
		this.notificationNumber = notificationNumber;
	}

	public ArrayList<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(ArrayList<Notification> notifications) {
		this.notifications = notifications;
	}
	
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
}
