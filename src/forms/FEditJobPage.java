package forms;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import models.bean.Ability;
import models.bean.Address;
import models.bean.Category;
import models.bean.Job;

public class FEditJobPage extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int jobID;

	private Job job;
	
	private ArrayList<Ability> abilities;
	
	private ArrayList<Category> categories;
	
	private ArrayList<Address> address;
	
	
	public int getJobID() {
		return jobID;
	}

	public void setJobID(int jobID) {
		this.jobID = jobID;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public ArrayList<Ability> getAbilities() {
		return abilities;
	}

	public void setAbilities(ArrayList<Ability> abilities) {
		this.abilities = abilities;
	}

	public ArrayList<Category> getCategories() {
		return categories;
	}

	public void setCategories(ArrayList<Category> categories) {
		this.categories = categories;
	}

	public ArrayList<Address> getAddress() {
		return address;
	}

	public void setAddress(ArrayList<Address> address) {
		this.address = address;
	}
	
}
