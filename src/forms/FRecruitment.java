package forms;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import models.bean.Ability;
import models.bean.Address;
import models.bean.Category;

public class FRecruitment extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ArrayList<Category> categories;
	private ArrayList<Address> address;
	private ArrayList<Ability> abilities;
	
	public ArrayList<Ability> getAbilities() {
		return abilities;
	}
	public void setAbilities(ArrayList<Ability> abilities) {
		this.abilities = abilities;
	}
	public ArrayList<Category> getCategories() {
		return categories;
	}
	public void setCategories(ArrayList<Category> categories) {
		this.categories = categories;
	}
	public ArrayList<Address> getAddress() {
		return address;
	}
	public void setAddress(ArrayList<Address> address) {
		this.address = address;
	}
	
}
