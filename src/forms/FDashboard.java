package forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import models.bean.Job;

public class FDashboard extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Job> listJob;

	public ArrayList<Job> getListJob() {
		return listJob;
	}

	public void setListJob(ArrayList<Job> listJob) {
		this.listJob = listJob;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

}
