package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.FJobDetail;
import models.bean.User;
import models.bo.BAbilityManagement;
import models.bo.BJobManagement;
import models.bo.BUserJobManagement;

public class AJobDetail extends Action {

	public ActionForward execute (ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) {
		
		FJobDetail fJobDetail = (FJobDetail) form;
		
		BJobManagement bJobManagement = new BJobManagement();
		BUserJobManagement bUserJobManagement = new BUserJobManagement();

		BAbilityManagement bAbilityManagement = new BAbilityManagement();
		
		HttpSession session = request.getSession();
		
		User us = (User) session.getAttribute("User");
		int jobID = Integer.parseInt(request.getParameter("Job"));
		
		if (us != null) {
			
			fJobDetail.setJob(bJobManagement.getJobDetail(jobID, us.getId()));
			
			fJobDetail.setApplied(bUserJobManagement.existUserJob(us.getId(), jobID));
			
			fJobDetail.setIsPublish(bJobManagement.isPublishJob(jobID));
			
		}
		else
		{
			fJobDetail.setJob(bJobManagement.getJobDetail(jobID));
		}
		
		fJobDetail.setAppliedNumber(bUserJobManagement.countAcceptUserJob(jobID));
		
		fJobDetail.setUserAppliedList(bUserJobManagement.getUserAppliedListByJobID(jobID));
		
		fJobDetail.setAbilities(bAbilityManagement.getAbilityList());
		
		return mapping.findForward("success");
		
	}
	
}
