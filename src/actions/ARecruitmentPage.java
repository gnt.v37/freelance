package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.FRecruitment;
import models.bo.BAbilityManagement;
import models.bo.BAddressManagement;
import models.bo.BCategoryManagement;

public class ARecruitmentPage extends Action  {

	public ActionForward execute (ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) {
		
		FRecruitment fRecruiment = (FRecruitment) form;
		
		BCategoryManagement bCategoryManagement = new BCategoryManagement();
		BAddressManagement bAddressManagement = new BAddressManagement();
		BAbilityManagement bAbilityManagement = new BAbilityManagement();
		
		fRecruiment.setCategories(bCategoryManagement.getCategoryList());

		fRecruiment.setAddress(bAddressManagement.getAddressList());
		
		fRecruiment.setAbilities(bAbilityManagement.getAbilityList());
		
		return mapping.findForward("success");
		
	}
}
