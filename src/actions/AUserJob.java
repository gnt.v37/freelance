package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.FUserJob;
import models.bean.User;
import models.bo.BJobManagement;

public class AUserJob extends Action {

	public ActionForward execute (ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) {
		
		FUserJob fUserJob = (FUserJob) form;
		
		BJobManagement bJobManagement = new BJobManagement();
		
		HttpSession session = request.getSession();
		
		User user = (User) session.getAttribute("User");
		
		int userID = user.getId();
		
		fUserJob.setJobs(bJobManagement.getJobListByUser(userID));
		
		return mapping.findForward("success");
		
	}
}
