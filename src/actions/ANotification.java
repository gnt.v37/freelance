package actions;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.FNotification;
import models.bean.User;
import models.bo.BNotificationManagement;

public class ANotification extends Action {

	public ActionForward execute (ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) {
		
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		response.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		
		User user = (User)session.getAttribute("User");
		FNotification fHeader = (FNotification) form;
		
		if (user != null) {
			BNotificationManagement bNotificationManagement = new BNotificationManagement();
			fHeader.setNotifications(bNotificationManagement.getNotificationByUser(user.getId()));
		}
		
		return mapping.findForward("success");
		
	}
	
}
