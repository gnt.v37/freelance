package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import models.bean.User;
import models.bo.BNotificationManagement;

public class AReadNotification extends Action {

	public ActionForward execute (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		BNotificationManagement bNotificationManagement = new BNotificationManagement();
		
		HttpSession session = request.getSession();
		
		User us = (User) session.getAttribute("User");
		
		bNotificationManagement.readNotification(us.getId());
		
		return mapping.findForward("success");
	
	}
	
}
