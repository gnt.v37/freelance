package actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.FDashboard;
import models.bean.Job;
import models.bo.BJobManagement;

public class ADashboard extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		FDashboard fDashboard = (FDashboard) form;
		
		HttpSession session = request.getSession();
		if (session.getAttribute("User") != null) {
			System.out.println("OK 1");
			BJobManagement bJobManagement = new BJobManagement();

			ArrayList<Job> listJobs = bJobManagement.getItems();
			System.out.println("OK 2");
			System.out.println("Size " + listJobs.size());
			fDashboard.setListJob(listJobs);

			System.out.println("OK 3");
			return mapping.findForward("success");
		} else {
			return mapping.findForward("failure");
		}
	}
}
