package actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.FHomePage;
import models.bean.Job;
import models.bo.BCategoryManagement;
import models.bo.BJobManagement;

public class ASearch extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		FHomePage fHomePage = (FHomePage) form;

		BJobManagement bJobManagement = new BJobManagement();

		BCategoryManagement bCategoryManagement = new BCategoryManagement();

		if (fHomePage.getCategoryID() != null) {

			int id = Integer.parseInt(fHomePage.getCategoryID());

			ArrayList<Job> jobList = bJobManagement.getJobListByCategory(id);

			if (jobList == null || jobList.size() == 0) {
				fHomePage.setMessage("Không tìm thấy dữ liệu");
			}

			System.out.println(id + "C");

			fHomePage.setJobList(jobList);
		} else if (fHomePage.getAll() != null) {

			String all = fHomePage.getAll();

			System.out.println(all + "A");

			ArrayList<Job> jobList = bJobManagement.getJobListByAll(all);

			if (jobList == null || jobList.size() == 0) {
				fHomePage.setMessage("Không tìm thấy dữ liệu");
			}

			fHomePage.setJobList(jobList);

		} else if (fHomePage.getMoneyID() != null) {
			int id = Integer.parseInt(fHomePage.getMoneyID());

			ArrayList<Job> jobList = bJobManagement.getJobListByMoney(id);

			if (jobList == null || jobList.size() == 0) {
				fHomePage.setMessage("Không tìm thấy dữ liệu");
			}

			fHomePage.setJobList(jobList);
		}
		else if (fHomePage.getAbilityID() != null) {
			int id = Integer.parseInt(fHomePage.getAbilityID());

			ArrayList<Job> jobList = bJobManagement.getJobListByAbility(id);

			if (jobList == null || jobList.size() == 0) {
				fHomePage.setMessage("Không tìm thấy dữ liệu");
			}

			fHomePage.setJobList(jobList);
		}

		fHomePage.setCategoryList(bCategoryManagement.getCategoryList());
		return mapping.findForward("success");

	}

}
