package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.FLogin;
import models.bo.BUserManagement;

public class ALoginProcess extends Action {

	public ActionForward execute (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		FLogin fLogin = (FLogin) form;
		
		BUserManagement bUserManagement = new BUserManagement();
		
		System.out.println(fLogin.getEmail());
		
		if (bUserManagement.loginProcess(request, fLogin.getEmail(), fLogin.getPassword())) {
			return mapping.findForward("success");
		}
		
		fLogin.setAccountError("Tài khoản hoặc mật khẩu không đúng.");
		
		return mapping.findForward("failure");
		
	}
}
