package actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.FUserForm;
import models.bean.User;
import models.bo.BUserManagement;

public class AUser extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		FUserForm fUserForm = (FUserForm) form;

		HttpSession session = request.getSession();
		if (session.getAttribute("User") != null) {

			BUserManagement bUserManagement = new BUserManagement();

			ArrayList<User> listUser = bUserManagement.getItems();
			System.out.println(listUser.size() + " UserSize ");
			
			fUserForm.setListUser(listUser);

			return mapping.findForward("success");
		} else {
			return mapping.findForward("failure");
		}
	}
}
