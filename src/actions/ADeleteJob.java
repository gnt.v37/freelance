package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.FJobPublish;
import models.bo.BJobManagement;

public class ADeleteJob extends Action {

	public ActionForward execute (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		FJobPublish fJobPublish = (FJobPublish) form;
		
		BJobManagement bJobManagement = new BJobManagement();
		
		boolean success = bJobManagement.deleteJob(fJobPublish.getJobID());
		
		
		if (success) {
			return mapping.findForward("success");
		}
		
		return mapping.findForward("failure");
		
	}
	
}
