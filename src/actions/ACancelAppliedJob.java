package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import forms.FApplyJob;
import models.bo.BUserJobManagement;

public class ACancelAppliedJob extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse resposne) {

		FApplyJob fApplyJob = (FApplyJob) form;
		BUserJobManagement bUserJobManagement = new BUserJobManagement();

		bUserJobManagement.deleteAppliedUser(fApplyJob.getUserID(), fApplyJob.getJobID());

		ActionRedirect redirect;

		redirect = new ActionRedirect(mapping.findForward("success"));

		redirect.addParameter("Job", fApplyJob.getJobID());

		return redirect;

		
	}

}
