package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import forms.FApplyJob;
import models.bo.BUserJobManagement;

public class ACancelUser extends Action {

	public ActionForward execute (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		FApplyJob fApplyJob = (FApplyJob) form;
		BUserJobManagement bUserJobManagement = new BUserJobManagement();
		
		System.out.println(fApplyJob.getUserID());
		
		System.out.println(fApplyJob.getJobID() + "A");
		
		bUserJobManagement.cancelUser(fApplyJob.getUserID(), fApplyJob.getJobID(), 2);
		
		ActionRedirect redirect = new ActionRedirect(mapping.findForwardConfig("success"));
		
		redirect.addParameter("Job", fApplyJob.getJobID());
		
		return redirect;
	}
}
