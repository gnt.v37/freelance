package actions;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.FNotification;
import models.bean.User;
import models.bo.BNotificationManagement;

public class ANotificationNumber extends Action {
	public ActionForward execute (ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) {
		
	
		HttpSession session = request.getSession();
		
		User user = (User)session.getAttribute("User");
		FNotification fHeader = (FNotification) form;
		System.out.println("235 OKOKO");
		if (user != null) {
			BNotificationManagement bNotificationManagement = new BNotificationManagement();
			fHeader.setNotificationNumber(bNotificationManagement.countNotification(user.getId()));
		}
		
		return mapping.findForward("success");
		
	}

}
