package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.FHomePage;
import models.bo.BCategoryManagement;
import models.bo.BJobManagement;

public class AHomePage extends Action {

	public ActionForward execute (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	{
		FHomePage fHomePage = (FHomePage)form;
		
		BJobManagement bJobManagement = new BJobManagement();
		
		BCategoryManagement bCategoryManagement = new BCategoryManagement();
		
		fHomePage.setJobList(bJobManagement.getJobList());

		fHomePage.setCategoryList(bCategoryManagement.getCategoryList());
		
		return mapping.findForward("success");
	}
}
