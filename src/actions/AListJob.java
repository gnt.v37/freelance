package actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.FListJob;
import models.bean.User;
import models.bean.UserJob;
import models.bo.BUserJobManagement;

public class AListJob extends Action{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		FListJob fListJob = (FListJob) form;
		
		BUserJobManagement dUserJobManagement = new BUserJobManagement();
		
		HttpSession session = request.getSession();
		
		User user = (User) session.getAttribute("User");
		
		int userID = user.getId();
		
		ArrayList<UserJob> listJob = dUserJobManagement.getJobAppliedListByUserID(userID);
		
		fListJob.setListJob(listJob);
		
		return mapping.findForward("success");
	}


}
