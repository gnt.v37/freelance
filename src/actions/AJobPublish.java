package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.FJobPublish;
import models.bo.BJobManagement;

public class AJobPublish extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		FJobPublish fJobPublish = (FJobPublish) form;
		
		BJobManagement bJobmanagement = new BJobManagement();

		System.out.println(fJobPublish.getJobName());
		System.out.println("A" + fJobPublish.getUserID());
		
		
		boolean success = bJobmanagement.addJob(fJobPublish.getJobName(), fJobPublish.getUserID(), fJobPublish.getCategoryID(),
				fJobPublish.getJobMoney(), fJobPublish.getJobDescription(), fJobPublish.getAddressID(),
				fJobPublish.getJobApplicationExpire(), fJobPublish.getAbilities());

		if (success) {
			return mapping.findForward("success");
			
		}
		
		return mapping.findForward("failure");

	}

}
