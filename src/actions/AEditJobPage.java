package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.FEditJobPage;
import models.bean.User;
import models.bo.BAbilityManagement;
import models.bo.BAddressManagement;
import models.bo.BCategoryManagement;
import models.bo.BJobManagement;

public class AEditJobPage extends Action {

	public ActionForward execute (ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) {
		
		FEditJobPage editJobPage = (FEditJobPage) form;
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("User");
		
		BJobManagement bJobManagement = new BJobManagement();
		BCategoryManagement bCategoryManagement = new BCategoryManagement();
		BAddressManagement bAddressManagement = new BAddressManagement();
		BAbilityManagement bAbilityManagement = new BAbilityManagement();
		
		
		editJobPage.setJob(bJobManagement.getJobDetail(editJobPage.getJobID(), user.getId()));
		System.out.println("OKOKSSSSSSSSSSSSS 2");
		editJobPage.setAbilities(bAbilityManagement.getAbilityList());
		System.out.println("OKOKSSSSSSSSSSSSS 3");
		editJobPage.setAddress(bAddressManagement.getAddressList());
		System.out.println("OKOKSSSSSSSSSSSSS 4");
		editJobPage.setCategories(bCategoryManagement.getCategoryList());
		
		System.out.println("OKOKSSSSSSSSSSSSS 5");
		return mapping.findForward("success");
		
		
	}
}
