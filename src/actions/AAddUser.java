package actions;

import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.FUserForm;
import models.bean.Address;
import models.bean.User;
import models.bo.BAddressManagement;
import models.bo.BUserManagement;

public class AAddUser extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		FUserForm fUserForm = (FUserForm) form;

		HttpSession session = request.getSession();
		
		if (session.getAttribute("User") != null) {
			
			BAddressManagement bAddressManagement = new BAddressManagement();
			BUserManagement bUserManagement = new BUserManagement();
			
			ArrayList<Address> listAddress = bAddressManagement.getAddressList();
			fUserForm.setListAddress(listAddress);

			if ("submit".equals(fUserForm.getSubmit())) {
				String firstName = fUserForm.getFirstname();
				String lastName = fUserForm.getLastname();
				String phone = fUserForm.getPhone();
				String email = fUserForm.getEmail();
				String passWord = fUserForm.getPassWord();
				int addressID = Integer.parseInt(fUserForm.getAddress());

				Address address = new Address(addressID, null);
				String avatar = fUserForm.getAvatar();
				Date created = new Date(new Date().getTime());

				User user = new User(firstName, lastName, phone, email, passWord, created, avatar, address);
				if (bUserManagement.addItem(user) > 0) {
					System.out.println("them thanh cong");
					return mapping.findForward("success");
				} else {
					ArrayList<Address> listAddress1 = bAddressManagement.getAddressList();
					fUserForm.setListAddress(listAddress1);
					System.out.println("Them that bai");
					return mapping.findForward("failure");
				}
			} else {
				return mapping.findForward("failure");
			}
		}
		return mapping.findForward("failure");

	}
}
