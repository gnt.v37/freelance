package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import forms.FApplyJob;
import models.bo.BUserAbilityManagement;
import models.bo.BUserJobManagement;

public class AApplyJob extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		FApplyJob fApplyJob = (FApplyJob) form;

		BUserJobManagement bUserJobManagement = new BUserJobManagement();
		BUserAbilityManagement bUserAbilityManagement = new BUserAbilityManagement();
		
		boolean success = bUserJobManagement.applyJob(fApplyJob.getUserID(), fApplyJob.getJobID(), fApplyJob.getMoney(),
				fApplyJob.getFinishedIn(), fApplyJob.getDescription(), fApplyJob.getConvince(), fApplyJob.getFacebook(),
				fApplyJob.getPhone(), fApplyJob.getEmail());
		
		System.out.println(fApplyJob.getAbilities());
		
		bUserAbilityManagement.addUserAbility(fApplyJob.getUserID(), fApplyJob.getAbilities());
		
		ActionRedirect redirect;

		if (success) {
			redirect = new ActionRedirect(mapping.findForward("success"));

			redirect.addParameter("Job", fApplyJob.getJobID());

			return redirect;
		}

		redirect = new ActionRedirect(mapping.findForward("failure"));

		return redirect;

	}
}
