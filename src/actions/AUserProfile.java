package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.FUserProfile;
import models.bo.BUserManagement;

public class AUserProfile extends Action {

	public ActionForward execute (ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) {
		
		FUserProfile fUserProfile = (FUserProfile) form;
		BUserManagement bUserManagement = new BUserManagement();
		
		int userID = Integer.parseInt(request.getParameter("User"));
		
		fUserProfile.setUser(bUserManagement.getUserByID(userID));
		
		return mapping.findForward("success");
	}
	
}
