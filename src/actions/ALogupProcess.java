package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.FLogup;
import models.bo.BUserManagement;

public class ALogupProcess extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		FLogup fLogup = (FLogup) form;

		BUserManagement bUserManagement = new BUserManagement();

		int success = bUserManagement.logupProcess(request, fLogup.getFirstname(), fLogup.getLastname(),
				fLogup.getEmail(), fLogup.getPassword());

		if (success == 1) {
			return mapping.findForward("success");
		}
		else if (success == 2) {
			fLogup.setAccountError("Email của bạn vừa nhập đã tồn tại.");
		}
		return mapping.findForward("failure");
	}

}
