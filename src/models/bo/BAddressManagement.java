package models.bo;

import java.util.ArrayList;

import models.bean.Address;
import models.dao.DAddressManagement;

public class BAddressManagement {
	
	private DAddressManagement dAddress;
	
	public BAddressManagement () {
		this.dAddress = new DAddressManagement();
	}
	
	public ArrayList<Address> getAddressList () {
		return this.dAddress.getAddressList();
	}

}
