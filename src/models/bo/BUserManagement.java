package models.bo;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import models.bean.User;
import models.dao.DUserManagement;

public class BUserManagement {

	private DUserManagement dUser;
	
	public BUserManagement () {
		dUser = new DUserManagement();
	}
	
	public boolean loginProcess (HttpServletRequest request, String email, String password) {
		return this.dUser.loginProcess(request, email, password);
				
	}
	
	public int logupProcess(HttpServletRequest request, String firstname, String lastname, String email, String password) {
		return this.dUser.logupProcess(request, firstname, lastname, email, password);
	}
	
	public boolean isEmailExisted (String email) {
		return this.dUser.isEmailExisted(email);
	}
	
	
	public User getUserByID (int userID) {
		return this.dUser.getUserByID(userID);
	}
	
	public ArrayList<User> getItems () {
		return this.dUser.getItems();
	}
	
	public int addItem(User user) {
		return this.dUser.addItem(user);
	}
	
}
