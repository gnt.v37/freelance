package models.bo;

import java.util.ArrayList;

import models.bean.Category;
import models.dao.DCategoryManagement;

public class BCategoryManagement {
	
	private DCategoryManagement dCategory;
	
	public BCategoryManagement () {
		dCategory = new DCategoryManagement();
	}
	
	public ArrayList<Category> getCategoryList () {
		
		return dCategory.getCategoryList();
		
	}
	
}
