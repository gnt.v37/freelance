package models.bo;

import java.util.ArrayList;

import models.bean.Notification;
import models.dao.DNotificationManagement;

public class BNotificationManagement {

	private DNotificationManagement dNotification;
	
	public BNotificationManagement () {
		this.dNotification = new DNotificationManagement();
	}
	
	public ArrayList<Notification> getNotificationByUser (int userID) {
		return this.dNotification.getNotificationByUser(userID);
	}
	
	public boolean readNotification (int userID) {
		return this.dNotification.readNotification(userID);
	}
	
	public int countNotification(int userID) {
		return this.dNotification.countNotification(userID);
	}
	
}
