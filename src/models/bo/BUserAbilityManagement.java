package models.bo;

import models.dao.DUserAbilityManagement;

public class BUserAbilityManagement {

	private DUserAbilityManagement dUserAbility;
	
	public BUserAbilityManagement () {
		this.dUserAbility = new DUserAbilityManagement();
	}
	
	public boolean addUserAbility (int userID, String abilities) {
		return this.dUserAbility.addUserAbility(userID, abilities);
	}
	
}
