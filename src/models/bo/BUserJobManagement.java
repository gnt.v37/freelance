package models.bo;

import java.util.ArrayList;

import models.bean.UserJob;
import models.dao.DUserJobManagement;

public class BUserJobManagement {
	private DUserJobManagement dUserJob;
	
	public BUserJobManagement () {
		this.dUserJob = new DUserJobManagement();
	}
	
	public boolean applyJob (int userID, int jobID, double money, String finishedIn, String description, String convince,
			String facebook, int phone, String email) {
		return this.dUserJob.applyJob(userID, jobID, money, finishedIn, description, convince, facebook, phone, email);
	}
	
	public ArrayList<UserJob> getUserAppliedListByJobID (int jobID) {
		return this.dUserJob.getUserAppliedListByJobID(jobID);
	}
	
	public int existUserJob (int userID, int jobID) {
		if (this.dUserJob.existUserJob(userID, jobID)) {
			return 1;
		}
		return 0;
	}
	
	public int countAcceptUserJob (int jobID) {
		return this.dUserJob.countAcceptUserJob(jobID);
	}
	
	public boolean acceptUser (int userID, int jobID, int isAccept) {
		return this.dUserJob.acceptUser(userID, jobID, isAccept);
	}
	
	public boolean cancelUser (int userID, int jobID, int isAccept) {
		return this.dUserJob.cancelUser(userID, jobID, isAccept);
	}
	
	public boolean deleteAppliedUser (int userID, int jobID) {
		return this.dUserJob.deleteAppliedUser(userID, jobID);
		
	}
	
	public boolean updateAppliedUser(int userID, int jobID, double money, String finishedIn, String description, String convince,
			String facebook, int phone, String email) {
		return this.dUserJob.updateAppliedUser(userID, jobID, money, finishedIn, description, convince, facebook, phone, email);
	}
	
	public ArrayList<UserJob> getJobAppliedListByUserID(int userID) {
		return this.dUserJob.getJobAppliedListByUserID(userID);
	}
	
}
