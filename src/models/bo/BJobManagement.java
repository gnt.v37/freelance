package models.bo;

import java.sql.Date;
import java.util.ArrayList;

import models.bean.Ability;
import models.bean.Job;
import models.dao.DJobManagement;

public class BJobManagement {

	private DJobManagement dJob;
	
	public BJobManagement () {
		dJob = new DJobManagement();
	}
	
	public ArrayList<Job> getJobList () {
		return dJob.getJobList();
	}
	
	public ArrayList<Job> getJobListByCategory(int categoryID) {
		return dJob.getJobListByCategory(categoryID);
	}
	
	public ArrayList<Job> getJobListByAll(String all) {
		return dJob.getJobListByAll(all);
	}
	
	public ArrayList<Job> getJobListByMoney(int moneyID) {
		return dJob.getJobListByMoney(moneyID);
	}
	
	
	public Job getJobDetail(int jobID) {
		return dJob.getJobDetail(jobID);
	}
	
	public Job getJobDetail(int jobID, int userID) {
		return dJob.getJobDetail(jobID, userID);
	}
	
	public ArrayList<Ability> getJobRequirement (int jobID) {
		return dJob.getJobRequirement(jobID);
	}
	
	public ArrayList<Job> getJobListByUser (int userID) {
		return this.dJob.getJobListByUser(userID);
	} 
	
	public ArrayList<Job> getJobListByAbility (int abilityID) {
		return this.dJob.getJobListByAbility(abilityID);
	}
	
	public boolean addJob(String name, int userID, int categoryID, double jobMoney, String jobDescription,
			int addressID, Date jobApplicationExpire, String abilities) {
		return this.dJob.addJob(name, userID, categoryID, jobMoney, jobDescription, addressID, jobApplicationExpire, abilities);
	}
	
	public boolean udateJob(String name, int jobID, int categoryID, double jobMoney, String jobDescription,
			int addressID, Date jobApplicationExpire) {
		return this.dJob.updateJob(name, jobID, categoryID, jobMoney, jobDescription, addressID, jobApplicationExpire);
	}
	
	public boolean deleteJob(int jobID) {
		return this.dJob.deleteJob(jobID);
	}
	
	public boolean startJob(int jobID) {
		return this.dJob.startJob(jobID);
	}
	
	public int isPublishJob (int jobID) {
		return this.dJob.isPublishJob(jobID);
	}
	
	
	// Admin
	
	
	public ArrayList<Job> getItems () {
		return this.dJob.getItems();
	}
	
}
