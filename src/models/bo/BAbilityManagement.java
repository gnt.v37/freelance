package models.bo;

import java.util.ArrayList;

import models.bean.Ability;
import models.dao.DAbilityManagement;

public class BAbilityManagement {

	private DAbilityManagement dAbility;
	
	public BAbilityManagement () {
		dAbility = new DAbilityManagement();
	}
	
	public ArrayList<Ability> getAbilityList () {
		return this.dAbility.getAbilityList();
	}
	
	
}
