package models.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import models.bean.Ability;
import models.bean.Address;
import models.bean.Job;
import models.bean.User;

public class DUserManagement {

	private Connection Connect;

	public DUserManagement() {
		Connect = DJobDatabase.Connect;
	}

	public boolean isEmailExisted(String email) {
		String Query = "SELECT user_id FROM tbl_Users WHERE user_email = ?";

		try {
			PreparedStatement pStmt = Connect.prepareStatement(Query);

			pStmt.setString(1, email);

			ResultSet rs = pStmt.executeQuery();

			if (rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public boolean loginProcess(HttpServletRequest request, String email, String password) {

		String Query = "SELECT * FROM tbl_Users WHERE user_email = ? AND user_password = ?";

		try {
			PreparedStatement pStmt = Connect.prepareStatement(Query);

			pStmt.setString(1, email);
			pStmt.setString(2, password);

			ResultSet rs = pStmt.executeQuery();

			if (rs.next()) {
				HttpSession session = request.getSession();
				session.setAttribute("User", new User(rs.getInt("user_id"), rs.getString("user_firstname"),
						rs.getString("user_lastname"), rs.getString("user_avatar"), rs.getInt("user_is_admin")));
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return false;
	}

	public int logupProcess(HttpServletRequest request, String firstname, String lastname, String email,
			String password) {

		if (this.isEmailExisted(email)) {
			return 2;
		}

		String Query = "INSERT INTO [dbo].[tbl_Users] ([user_firstname], [user_lastname],"
				+ " [user_email], [user_password]  ,[user_created]) VALUES (?, ?, ?, ?, GETDATE())";

		try {
			PreparedStatement pStmt = Connect.prepareStatement(Query);

			pStmt.setString(1, firstname);
			pStmt.setString(2, lastname);
			pStmt.setString(3, email);
			pStmt.setString(4, password);

			int success = pStmt.executeUpdate();

			if (success == 1) {
				System.out.println("Thanh Cong");
				this.loginProcess(request, email, password);
				return 1;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return 0;
	}

	public User getUserByID(int userID) {

		String Query = "SELECT * FROM tbl_Users us " + " INNER JOIN tbl_Address ad ON us.address_id = ad.address_id"
				+ " WHERE user_id = ?";

		try {
			PreparedStatement pStmt = this.Connect.prepareStatement(Query);

			pStmt.setInt(1, userID);

			ResultSet rs = pStmt.executeQuery();

			if (rs.next()) {
				return new User(rs.getInt("user_id"), rs.getInt("user_job_applied"), rs.getInt("user_job_recruited"),
						rs.getString("user_firstname"), rs.getString("user_lastname"),
						new Address(rs.getInt("address_id"), rs.getString("address_name")),
						rs.getString("user_description"), rs.getString("user_phone"), rs.getString("user_title"),
						rs.getString("user_email"), rs.getString("user_password"), rs.getString("user_avatar"),
						this.getUserAbilities(userID), this.getUserJobs(userID),
						new Date(rs.getDate("user_created").getTime()));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return null;
	}

	private ArrayList<Job> getUserJobs(int userID) {
		ArrayList<Job> userJobs = new ArrayList<>();

		String Query = "SELECT us.user_id, us.user_firstname, us.user_lastname, us.user_avatar, jo.job_id, jo.job_name, jo.job_money FROM tbl_UserJobs ujo "
				+ " INNER JOIN tbl_Jobs jo ON ujo.job_id = jo.job_id"
				+ " INNER JOIN tbl_Users us ON us.user_id = jo.user_id" + " WHERE ujo.user_id = ?";

		try {
			PreparedStatement pStmt = this.Connect.prepareStatement(Query);

			pStmt.setInt(1, userID);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				userJobs.add(new Job(
						rs.getInt("job_id"), new User(rs.getInt("user_id"), rs.getString("user_firstname"),
								rs.getString("user_lastname"), rs.getString("user_avatar")),
						rs.getString("job_name"), rs.getDouble("job_money")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return userJobs;
	}

	public ArrayList<Ability> getUserAbilities(int userID) {

		ArrayList<Ability> userAbilities = new ArrayList<>();

		String Query = "SELECT ab.ability_id, ab.ability_name FROM tbl_UserAbilities uab"
				+ " INNER JOIN tbl_Abilities ab ON uab.ability_id = ab.ability_id" + " WHERE uab.user_id = ?";

		try {
			PreparedStatement pStmt = this.Connect.prepareStatement(Query);

			System.out.println("Hello User M " + userID);

			pStmt.setInt(1, userID);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				System.out.println(rs.getString(2));
				userAbilities.add(new Ability(rs.getInt(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

		return userAbilities;

	}

	// Admin

	public ArrayList<User> getItems() {

		ArrayList<User> items = new ArrayList<User>();

		String sql = "select * from tbl_Users u inner join tbl_Address a" + " on u.address_id = a.address_id"
				+ " order by user_id desc";
		try {
			Statement stmt = this.Connect.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int userId = rs.getInt("user_id");
				String userFirstName = rs.getString("user_firstname");
				String userLastName = rs.getString("user_lastname");
				String userDescription = rs.getString("user_description");
				String phone = rs.getString("user_phone");
				String email = rs.getString("user_email");
				String passWord = rs.getString("user_password");
				String title = rs.getString("user_title");
				int userJobApply = rs.getInt("user_job_applied");
				int userJobRecruit = rs.getInt("user_job_recruited");
				Date userCreate = rs.getTimestamp("user_created");
				String userAvatar = rs.getString("user_avatar");
				Address addressId = new Address(rs.getInt("address_id"), rs.getString("address_name"));

				User u = new User(userId, userFirstName, userLastName, userDescription, phone, email, passWord, title,
						userJobApply, userJobRecruit, userCreate, userAvatar, addressId);
				items.add(u);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		return items;
	}

	public int addItem(User user) {
		int result = 0;
		String sql = "insert into tbl_Users(user_firstname,user_lastname,user_phone,user_email,user_password,user_created,user_avatar,address_id)"
				+ " values(?,?,?,?,?,?,?,?)";
		try {
			PreparedStatement pStmt = this.Connect.prepareStatement(sql);
			pStmt.setString(1, user.getFirstName());
			pStmt.setString(2, user.getLastName());
			pStmt.setString(3, user.getPhone());
			pStmt.setString(4, user.getEmail());
			pStmt.setString(5, user.getPassword());
			pStmt.setDate(6, new java.sql.Date(user.getCreated().getTime()));
			pStmt.setString(7, user.getAvatar());
			pStmt.setInt(8, user.getAddress().getId());
			result = pStmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

}
