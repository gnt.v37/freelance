package models.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import models.bean.Notification;

public class DNotificationManagement {

	private Connection Connect;

	public DNotificationManagement() {
		this.Connect = DJobDatabase.Connect;
	}

	public ArrayList<Notification> getNotificationByUser(int userID) {
		new DJobDatabase();
		this.Connect = DJobDatabase.Connect;
		ArrayList<Notification> notification = new ArrayList<>();

		String query = "SELECT * FROM tbl_Notification WHERE user_id = ? ORDER BY notification_time DESC";
		System.out.println(userID);
		try {
			PreparedStatement pStmt = this.Connect.prepareStatement(query);

			pStmt.setInt(1, userID);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				System.out.println("HIHIHIHIHI");
				notification
						.add(new Notification(rs.getInt("notification_id"), rs.getInt("job_id"), rs.getInt("user_id"),
								rs.getInt("is_read"), rs.getString("notification"), rs.getDate("notification_time")));

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

		return notification;
	}

	public int countNotification(int userID) {
		new DJobDatabase();
		this.Connect = DJobDatabase.Connect;
		String query = "SELECT COUNT(notification_id) FROM tbl_Notification WHERE user_id = ? AND is_read = 0";
		System.out.println(userID);
		try {
			PreparedStatement pStmt = this.Connect.prepareStatement(query);

			pStmt.setInt(1, userID);

			ResultSet rs = pStmt.executeQuery();

			if (rs.next()) {
				return rs.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

		return -1;

	}

	public boolean readNotification(int userID) {
		
		String query = "UPDATE tbl_Notification SET is_read = 1 WHERE user_id = ?";
		new DJobDatabase();
		this.Connect = DJobDatabase.Connect;
		try {
			PreparedStatement pStmt = this.Connect.prepareStatement(query);
			
			pStmt.setInt(1, userID);
			
			if (pStmt.executeUpdate() == 1) {
				return true;
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return false;
	}
}
