package models.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import models.bean.Ability;

public class DAbilityManagement {

	private Connection Connect;
	
	public DAbilityManagement () {
		this.Connect = DJobDatabase.Connect;
	}
	
	public ArrayList<Ability> getAbilityList () {
		ArrayList<Ability> abilityList = new ArrayList<>();
		
		String Query = "SELECT ability_id, ability_name FROM tbl_Abilities";
		
		try {
			PreparedStatement pStmt = Connect.prepareStatement(Query);
			
			ResultSet rs = pStmt.executeQuery();
			
			while (rs.next()) {
				abilityList.add(new Ability(rs.getInt(1), rs.getString(2)));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
		return abilityList;
		
	}
	
	
}
