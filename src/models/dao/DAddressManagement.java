package models.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import models.bean.Address;

public class DAddressManagement {
	
	private Connection Connect;
	
	public DAddressManagement () {
		Connect = DJobDatabase.Connect;
	}
	
	public ArrayList<Address> getAddressList () {
		ArrayList<Address> address = new ArrayList<>();
		
		String Query = "SELECT address_id, address_name FROM tbl_Address";
		
		try {
			PreparedStatement pStmt = Connect.prepareStatement(Query);
			
			ResultSet rs = pStmt.executeQuery();
			
			while (rs.next()) {
				address.add(new Address(rs.getInt(1), rs.getString(2)));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
		return address;
		
	}

}
