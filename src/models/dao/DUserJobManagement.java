package models.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import models.bean.Job;
import models.bean.User;
import models.bean.UserJob;

public class DUserJobManagement {

	private Connection Connect;

	public DUserJobManagement() {
		this.Connect = DJobDatabase.Connect;
	}

	public boolean applyJob(int userID, int jobID, double money, String finishedIn, String description, String convince,
			String facebook, int phone, String email) {

		String Query = "EXEC applyJob ?, ?, ?, ?, ?, ?, ?, ?, ?";

		try {
			PreparedStatement pStmt = this.Connect.prepareStatement(Query);

			pStmt.setInt(1, userID);
			pStmt.setInt(2, jobID);
			pStmt.setDouble(3, money);
			pStmt.setString(4, finishedIn);
			pStmt.setString(5, description);
			pStmt.setString(6, convince);
			pStmt.setString(7, facebook);
			pStmt.setInt(8, phone);
			pStmt.setString(9, email);

			if (pStmt.executeUpdate() == 1) {
				return true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		// CREATE TRIGGER incrementUserApply
		// ON tbl_UserJobs
		// FOR INSERT
		// AS BEGIN
		// DECLARE @appliedNumber int
		// SELECT @appliedNumber = jo.job_applied_number FROM tbl_Jobs jo
		// WHERE jo.job_id = (SELECT ins.job_id from inserted ins)
		//
		// UPDATE tbl_Jobs
		// SET job_applied_number = @appliedNumber + 1
		// WHERE job_id = (SELECT ins.job_id from inserted ins)
		//
		// END
		return false;
	}

	public boolean acceptUser(int userID, int jobID, int isAccept) {
		String Query = "EXEC applyUser ?, ?, ?";

		System.out.println(userID);
		System.out.println(jobID);

		try {

			PreparedStatement pStmt = this.Connect.prepareStatement(Query);

			pStmt.setInt(1, userID);
			pStmt.setInt(2, jobID);
			pStmt.setInt(3, isAccept);
			int success = pStmt.executeUpdate();

			if (success == 1) {
				return true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("77 " + e.getMessage());
		}

		return false;
	}

	public boolean cancelUser(int userID, int jobID, int isAccept) {
		String Query = "EXEC cancelUser ?, ?, ?";

		System.out.println(userID);
		System.out.println(jobID);

		try {

			PreparedStatement pStmt = this.Connect.prepareStatement(Query);

			pStmt.setInt(1, userID);
			pStmt.setInt(2, jobID);
			pStmt.setInt(3, isAccept);
			int success = pStmt.executeUpdate();

			if (success == 1) {
				return true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("77 " + e.getMessage());
		}

		return false;
	}

	public boolean existUserJob(int userID, int jobID) {

		String Query = "SELECT * FROM tbl_UserJobs uj WHERE uj.job_id = ? AND uj.user_id = ?";

		try {

			PreparedStatement pStmt = this.Connect.prepareStatement(Query);

			pStmt.setInt(1, jobID);
			pStmt.setInt(2, userID);

			ResultSet rs = pStmt.executeQuery();

			if (rs.next()) {
				return true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("77 " + e.getMessage());
		}

		return false;
	}

	public int countAcceptUserJob(int jobID) {

		String Query = "SELECT COUNT(uj.user_id) FROM tbl_UserJobs uj WHERE uj.job_id = ? AND uj.uj_is_accept = 1";

		try {

			PreparedStatement pStmt = this.Connect.prepareStatement(Query);

			pStmt.setInt(1, jobID);

			ResultSet rs = pStmt.executeQuery();

			if (rs.next()) {
				return rs.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("77 " + e.getMessage());
		}

		return 0;
	}

	public ArrayList<UserJob> getUserAppliedListByJobID(int jobID) {
		ArrayList<UserJob> userAppliedList = new ArrayList<>();

		String Query = "SELECT us.user_id, us.user_firstname, us.user_lastname, us.user_avatar,"
				+ " uj.uj_finished_in, uj.uj_description, uj.uj_convince, uj.uj_facebook, uj.uj_phone, uj.uj_email,"
				+ " uj.job_id, uj.uj_money, uj.uj_is_accept, uj.uj_apply_time, uj.uj_accept_time FROM tbl_UserJobs uj"
				+ " INNER JOIN tbl_Users us ON uj.user_id = us.user_id" + " WHERE uj.job_id = ?";

		DUserManagement umanager = new DUserManagement();
		try {

			PreparedStatement pStmt = this.Connect.prepareStatement(Query);

			pStmt.setInt(1, jobID);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				userAppliedList.add(new UserJob(
						new User(rs.getInt("user_id"), rs.getString("user_firstname"), rs.getString("user_lastname"),
								umanager.getUserAbilities(rs.getInt("user_id")), rs.getString("user_avatar")),
						rs.getDouble("uj_money"), rs.getString("uj_finished_in"), rs.getString("uj_description"),
						rs.getString("uj_convince"), rs.getString("uj_facebook"), rs.getString("uj_phone"),
						rs.getString("uj_email"), rs.getString("uj_is_accept"), rs.getTimestamp("uj_apply_time"),
						rs.getTimestamp("uj_accept_time")));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("77 " + e.getMessage());
		}

		return userAppliedList;

	}

	public ArrayList<UserJob> getJobAppliedListByUserID(int userID) {
		ArrayList<UserJob> jobAppliedList = new ArrayList<>();

		String Query = "SELECT j.job_id, j.job_name, uj.uj_finished_in, uj.uj_description, uj.uj_convince, uj.uj_facebook, uj.uj_phone, uj.uj_email, uj.uj_money,uj.uj_is_accept,uj.uj_apply_time,uj.uj_accept_time FROM tbl_UserJobs uj INNER JOIN tbl_Jobs j ON uj.job_id = j.job_id WHERE uj.user_id = ?";

		try {

			PreparedStatement pStmt = this.Connect.prepareStatement(Query);

			pStmt.setInt(1, userID);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				
				System.out.println("AAA " + rs.getString("uj_is_accept"));
				
				jobAppliedList.add(new UserJob(new Job(rs.getInt("job_id"), rs.getString("job_name")),
						rs.getDouble("uj_money"), rs.getString("uj_is_accept"), rs.getTimestamp("uj_apply_time"),
						rs.getTimestamp("uj_accept_time"), rs.getString("uj_finished_in"), rs.getString("uj_description"),
						rs.getString("uj_convince"), rs.getString("uj_facebook"), rs.getString("uj_phone"),
						rs.getString("uj_email")));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("77 " + e.getMessage());
		}

		return jobAppliedList;

	}

	public boolean deleteAppliedUser(int userID, int jobID) {
		String query = "DELETE tbl_UserJobs WHERE job_id = ? AND user_id = ?";

		try {
			PreparedStatement pStmt = this.Connect.prepareStatement(query);

			pStmt.setInt(1, jobID);
			pStmt.setInt(2, userID);

			int success = pStmt.executeUpdate();

			if (success == 1) {
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	public boolean updateAppliedUser(int userID, int jobID, double money, String finishedIn, String description,
			String convince, String facebook, int phone, String email) {
		String query = "UPDATE [dbo].[tbl_UserJobs]" + " SET [uj_money] = ?" + " ,[uj_finished_in] = ?"
				+ " ,[uj_description] = ?" + " ,[uj_convince] = ?" + " ,[uj_facebook] = ?" + " ,[uj_phone] = ?"
				+ " ,[uj_email] = ?" + " WHERE [user_id] = ? AND [job_id] = ?";
		System.out.println(userID + " userID");

		try {
			PreparedStatement pStmt = this.Connect.prepareStatement(query);

			pStmt.setDouble(1, money);
			pStmt.setString(2, finishedIn);
			pStmt.setString(3, description);
			pStmt.setString(4, convince);
			pStmt.setString(5, facebook);
			pStmt.setInt(6, phone);
			pStmt.setString(7, email);
			pStmt.setInt(8, userID);
			pStmt.setInt(9, jobID);

			if (pStmt.executeUpdate() == 1) {
				return true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		return false;
	}

}
