package models.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Hashtable;

public class DUserAbilityManagement {
	
	private Connection Connect;
	
	public DUserAbilityManagement () {
		this.Connect = DJobDatabase.Connect;
	}
	
	public boolean addUserAbility(int userID, String abilities) {
		Hashtable<String, String> hability = this.abilities(abilities);
		Enumeration<String> keys = hability.keys();

		while (keys.hasMoreElements()) {
			
			String key = keys.nextElement();

			String query = "EXEC addUserAbility ?, ?";

			try {
				PreparedStatement pStmt = this.Connect.prepareStatement(query);

				pStmt.setInt(1, userID);
				pStmt.setString(2, hability.get(key));

				pStmt.executeUpdate();
				System.out.println("OKOKOKOKOO");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			}
		}
		
		return true;
	}

	private Hashtable<String, String> abilities(String abilities) {
		String[] abilitiesSplit = abilities.split(", ");

		Hashtable<String, String> hability = new Hashtable<>();
		for (String ability : abilitiesSplit) {
			System.out.println(ability + " Ability");
			hability.put(ability, ability);
		}

		return hability;

	}
}
