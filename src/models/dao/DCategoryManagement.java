package models.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import models.bean.Category;

public class DCategoryManagement {

	private Connection Connect;
	
	public DCategoryManagement() {
		Connect = DJobDatabase.Connect;
	}
	
	public ArrayList<Category> getCategoryList ()
	{
		
		ArrayList<Category> categoryList = new ArrayList<>();
		
		String Query = "SELECT * FROM tbl_Categories";
		System.out.println("Hello");
		try {
			PreparedStatement pStmt = Connect.prepareStatement(Query);
			
			ResultSet rs = pStmt.executeQuery();
			
			
			while(rs.next()) {
				System.out.println(rs.getString(2));
				categoryList.add(new Category(rs.getInt(1), rs.getString(2)));
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
		return categoryList;
	}
	
}
