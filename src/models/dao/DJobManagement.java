package models.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import models.bean.Ability;
import models.bean.Address;
import models.bean.Category;
import models.bean.Job;
import models.bean.User;

public class DJobManagement {

	private Connection Connect;

	public DJobManagement() {
		new DJobDatabase();
		Connect = DJobDatabase.Connect;
	}

	public ArrayList<Job> getJobList() {
		ArrayList<Job> jobList = new ArrayList<>();

		String Query = "SELECT * FROM view_home ORDER BY job_id DESC";

		try {
			PreparedStatement pStmt = Connect.prepareStatement(Query);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				System.out.println(rs.getString("user_avatar"));
				jobList.add(new Job(rs.getInt("job_id"),
						new Category(rs.getInt("category_id"), rs.getString("category_name")),
						new User(rs.getInt("user_id"), rs.getString("user_firstname"), rs.getString("user_lastname"),
								rs.getString("user_avatar")),
						new Address(rs.getInt("address_id"), rs.getString("address_name")),
						this.getJobRequirement(rs.getInt("job_id")), rs.getString("job_name"),
						rs.getString("job_description"), rs.getDouble("job_money"),
						rs.getTimestamp("job_application_expire"), rs.getTimestamp("job_created"),
						rs.getInt("job_applied_number")));
				System.out.println();

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

		return jobList;

	}

	public Job getJobDetail(int jobID) {

		String job = " job_id = " + jobID;
		String orderBy = " ORDER BY job_id DESC";

		String Query = "SELECT * FROM view_job_detail WHERE job_is_publish = 1 AND";

		if (jobID <= 0) {
			Query += orderBy;
		} else {
			Query += job + orderBy;
		}

		try {
			PreparedStatement pStmt = Connect.prepareStatement(Query);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				System.out.println(rs.getString("job_name"));
				return new Job(rs.getInt("job_id"),
						new Category(rs.getInt("category_id"), rs.getString("category_name")),
						new User(rs.getInt("user_id"), rs.getInt("user_job_applied"), rs.getInt("user_job_recruited"),
								rs.getString("user_firstname"), rs.getString("user_lastname"),
								new Address(rs.getInt("user_address_id"), rs.getString("user_address_name")),
								rs.getString("user_phone"), rs.getString("user_email"),
								new Date(rs.getDate("user_created").getTime()), rs.getString("user_avatar")),
						new Address(rs.getInt("address_id"), rs.getString("address_name")),
						this.getJobRequirement(rs.getInt("job_id")), rs.getString("job_name"),
						rs.getString("job_description"), rs.getDouble("job_money"),
						rs.getTimestamp("job_application_expire"), rs.getTimestamp("job_created"),
						rs.getInt("job_applied_number"));

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

		return null;
	}

	public Job getJobDetail(int jobID, int userID) {

		String Query = "EXEC getJobDetail ?, ?";

		try {
			PreparedStatement pStmt = Connect.prepareStatement(Query);

			pStmt.setInt(1, jobID);
			pStmt.setInt(2, userID);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				System.out.println(rs.getString("job_name"));
				return new Job(rs.getInt("job_id"),
						new Category(rs.getInt("category_id"), rs.getString("category_name")),
						new User(rs.getInt("user_id"), rs.getInt("user_job_applied"), rs.getInt("user_job_recruited"),
								rs.getString("user_firstname"), rs.getString("user_lastname"),
								new Address(rs.getInt("user_address_id"), rs.getString("user_address_name")),
								rs.getString("user_phone"), rs.getString("user_email"),
								new Date(rs.getDate("user_created").getTime()), rs.getString("user_avatar")),
						new Address(rs.getInt("address_id"), rs.getString("address_name")),
						this.getJobRequirement(rs.getInt("job_id")), rs.getString("job_name"),
						rs.getString("job_description"), rs.getDouble("job_money"),
						rs.getTimestamp("job_application_expire"), rs.getTimestamp("job_created"),
						rs.getInt("job_applied_number"));

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

		return null;
	}

	public ArrayList<Job> getJobListByCategory(int categoryID) {
		ArrayList<Job> jobList = new ArrayList<>();

		String category = " WHERE category_id = " + categoryID;
		String orderBy = " ORDER BY job_id DESC";

		String Query = "SELECT * FROM view_home";
		if (categoryID <= 0) {
			Query += orderBy;
		} else {
			Query += category + orderBy;
		}

		try {
			PreparedStatement pStmt = Connect.prepareStatement(Query);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {

				jobList.add(new Job(rs.getInt("job_id"),
						new Category(rs.getInt("category_id"), rs.getString("category_name")),
						new User(rs.getInt("user_id"), rs.getString("user_firstname"), rs.getString("user_lastname"),
								rs.getString("user_avatar")),
						new Address(rs.getInt("address_id"), rs.getString("address_name")),
						this.getJobRequirement(rs.getInt("job_id")), rs.getString("job_name"),
						rs.getString("job_description"), rs.getDouble("job_money"),
						rs.getTimestamp("job_application_expire"), rs.getTimestamp("job_created"),
						rs.getInt("job_applied_number")));

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

		return jobList;
	}

	public ArrayList<Job> getJobListByMoney(int moneyID) {
		ArrayList<Job> jobList = new ArrayList<>();

		String money = "";
		System.out.println("MOney " + moneyID);
		switch (moneyID) {
		case 1:

			money = " WHERE job_money >= 200000000";
			break;
		case 2:

			money = " WHERE job_money < 200000000 AND job_money >= 50000000";
			break;
		case 3:

			money = " WHERE job_money < 50000000 AND job_money >= 10000000";
			break;
		case 4:

			money = " WHERE job_money < 10000000 AND job_money >= 5000000";
			break;
		case 5:

			money = " WHERE job_money < 5000000";
			break;
		default:
			break;
		}

		String orderBy = " ORDER BY job_money DESC";

		String Query = "SELECT * FROM view_home";
		if (moneyID <= 0) {
			Query += orderBy;
		} else {
			Query += money + orderBy;
		}

		try {
			PreparedStatement pStmt = Connect.prepareStatement(Query);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {

				jobList.add(new Job(rs.getInt("job_id"),
						new Category(rs.getInt("category_id"), rs.getString("category_name")),
						new User(rs.getInt("user_id"), rs.getString("user_firstname"), rs.getString("user_lastname"),
								rs.getString("user_avatar")),
						new Address(rs.getInt("address_id"), rs.getString("address_name")),
						this.getJobRequirement(rs.getInt("job_id")), rs.getString("job_name"),
						rs.getString("job_description"), rs.getDouble("job_money"),
						rs.getTimestamp("job_application_expire"), rs.getTimestamp("job_created"),
						rs.getInt("job_applied_number")));

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

		return jobList;
	}

	public ArrayList<Job> getJobListByAll(String all) {
		ArrayList<Job> jobList = new ArrayList<>();

		String Query = "EXEC search_home ?";

		try {
			PreparedStatement pStmt = Connect.prepareStatement(Query);

			pStmt.setString(1, all);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {

				jobList.add(new Job(rs.getInt("job_id"),
						new Category(rs.getInt("category_id"), rs.getString("category_name")),
						new User(rs.getInt("user_id"), rs.getString("user_firstname"), rs.getString("user_lastname"),
								rs.getString("user_avatar")),
						new Address(rs.getInt("address_id"), rs.getString("address_name")),
						this.getJobRequirement(rs.getInt("job_id")), rs.getString("job_name"),
						rs.getString("job_description"), rs.getDouble("job_money"),
						rs.getTimestamp("job_application_expire"), rs.getTimestamp("job_created"),
						rs.getInt("job_applied_number")));

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

		return jobList;
	}

	public ArrayList<Job> getJobListByUser(int userID) {
		ArrayList<Job> jobList = new ArrayList<>();

		String Query = "SELECT * FROM view_full_job" + "	WHERE user_id = ? " + " ORDER BY job_id DESC";

		try {
			PreparedStatement pStmt = Connect.prepareStatement(Query);

			pStmt.setInt(1, userID);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				System.out.println(rs.getString("user_avatar"));
				jobList.add(new Job(rs.getInt("job_id"),
						new Category(rs.getInt("category_id"), rs.getString("category_name")),
						new User(rs.getInt("user_id"), rs.getString("user_firstname"), rs.getString("user_lastname"),
								rs.getString("user_avatar")),
						new Address(rs.getInt("address_id"), rs.getString("address_name")),
						this.getJobRequirement(rs.getInt("job_id")), rs.getString("job_name"),
						rs.getString("job_description"), rs.getDouble("job_money"),
						rs.getTimestamp("job_application_expire"), rs.getTimestamp("job_created"),
						rs.getInt("job_applied_number"), rs.getInt("job_is_publish")));

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

		return jobList;

	}

	public ArrayList<Job> getJobListByAbility(int abilityID) {
		ArrayList<Job> jobList = new ArrayList<>();

		String Query = "SELECT vh.* FROM view_home vh" + " INNER JOIN tbl_JobRequirement jq" + " ON vh.job_id = jq.job_id"
				+ " WHERE jq.ability_id = ? ORDER BY job_id DESC";

		try {
			PreparedStatement pStmt = Connect.prepareStatement(Query);

			pStmt.setInt(1, abilityID);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				System.out.println(rs.getString("user_avatar"));
				jobList.add(new Job(rs.getInt("job_id"),
						new Category(rs.getInt("category_id"), rs.getString("category_name")),
						new User(rs.getInt("user_id"), rs.getString("user_firstname"), rs.getString("user_lastname"),
								rs.getString("user_avatar")),
						new Address(rs.getInt("address_id"), rs.getString("address_name")),
						this.getJobRequirement(rs.getInt("job_id")), rs.getString("job_name"),
						rs.getString("job_description"), rs.getDouble("job_money"),
						rs.getTimestamp("job_application_expire"), rs.getTimestamp("job_created"),
						rs.getInt("job_applied_number"), rs.getInt("job_is_publish")));

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

		return jobList;

	}

	public int isPublishJob(int jobID) {
		String Query = "SELECT job_is_publish FROM tbl_Jobs WHERE job_id = ?";

		try {
			PreparedStatement pStmt = this.Connect.prepareStatement(Query);

			pStmt.setInt(1, jobID);

			ResultSet rs = pStmt.executeQuery();
			if (rs.next()) {

				return rs.getInt(1);
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		return 2;
	}

	public ArrayList<Ability> getJobRequirement(int jobID) {
		ArrayList<Ability> jobRequirement = new ArrayList<>();

		String Query = "SELECT ab.ability_id, ab.ability_name FROM tbl_JobRequirement re "
				+ " INNER JOIN tbl_Abilities ab ON re.ability_id = ab.ability_id" + " WHERE re.job_id = ?";

		try {
			PreparedStatement pStmt = Connect.prepareStatement(Query);

			pStmt.setInt(1, jobID);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				jobRequirement.add(new Ability(rs.getInt("ability_id"), rs.getString("ability_name")));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

		return jobRequirement;

	}

	// Delete
	public boolean deleteJob(int jobID) {

		System.out.println(jobID);

		String Query = "EXEC deleteJob ?";

		try {
			PreparedStatement pStmt = this.Connect.prepareStatement(Query);

			pStmt.setInt(1, jobID);

			if (pStmt.executeUpdate() == 1) {

				return true;
			}

			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		return false;
	}

	// Update

	public boolean updateJob(String name, int jobID, int categoryID, double jobMoney, String jobDescription,
			int addressID, Date jobApplicationExpire) {

		String Query = "UPDATE tbl_Jobs"
				+ " SET job_name = ?, category_id = ?, job_money = ?, job_description = ?, address_id = ?, job_application_expire = ?"
				+ " WHERE job_id = ?";

		try {
			PreparedStatement pStmt = this.Connect.prepareStatement(Query);

			pStmt.setString(1, name);
			pStmt.setInt(2, categoryID);
			pStmt.setDouble(3, jobMoney);
			pStmt.setString(4, jobDescription);
			pStmt.setInt(5, addressID);
			pStmt.setDate(6, jobApplicationExpire);
			pStmt.setInt(7, jobID);

			pStmt.executeUpdate();

			if (pStmt.executeUpdate() == 1) {

				return true;
			}

			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		return false;
	}

	// Add

	public boolean updateAbilities(int jobID, String abilities) {

		Hashtable<String, String> hability = this.abilities(abilities);
		Enumeration<String> keys = hability.keys();

		while (keys.hasMoreElements()) {

			String key = keys.nextElement();

			String query = "INSERT INTO tbl_JobRequirement VALUES (?, ?)";

			try {
				PreparedStatement pStmt = this.Connect.prepareStatement(query);

				pStmt.setInt(1, jobID);
				pStmt.setString(2, hability.get(key));

				pStmt.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			}

		}

		return true;
	}

	public boolean addJob(String name, int userID, int categoryID, double jobMoney, String jobDescription,
			int addressID, Date jobApplicationExpire, String abilities) {

		String Query = "EXEC jobPublish ?, ?, ?, ?, ?, ?, ?";

		try {
			PreparedStatement pStmt = this.Connect.prepareStatement(Query);

			pStmt.setString(1, name);
			pStmt.setInt(2, userID);
			pStmt.setInt(3, categoryID);
			pStmt.setDouble(4, jobMoney);
			pStmt.setString(5, jobDescription);
			pStmt.setInt(6, addressID);
			pStmt.setDate(7, jobApplicationExpire);

			ResultSet rs = pStmt.executeQuery();

			if (rs.next()) {
				int jobID = rs.getInt(1);

				this.updateAbilities(jobID, abilities);

			}

			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		return false;
	}

	public boolean startJob(int jobID) {

		String Query = "EXEC startJob ?";

		try {
			PreparedStatement pStmt = this.Connect.prepareStatement(Query);

			pStmt.setInt(1, jobID);

			if (pStmt.executeUpdate() == 1) {

				return true;
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		return false;
	}

	private Hashtable<String, String> abilities(String abilities) {
		String[] abilitiesSplit = abilities.split(", ");

		Hashtable<String, String> hability = new Hashtable<>();
		for (String ability : abilitiesSplit) {
			System.out.println(ability + " Ability");
			hability.put(ability, ability);
		}

		return hability;

	}

	// Admin

	public ArrayList<Job> getItems() {
		ArrayList<Job> items = new ArrayList<Job>();

		String sql = "select * from tbl_Jobs j inner join tbl_Users u on j.user_id = u.user_id"
				+ " inner join tbl_Categories c on j.category_id = c.category_id"
				+ " inner join tbl_Address a on j.address_id=a.address_id" + " order by job_id desc";
		try {
			Statement st = this.Connect.createStatement();
			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				int jobId = rs.getInt("job_id");
				String jobName = rs.getString("job_name");

				User user = new User(rs.getInt("user_id"), rs.getString("user_firstname"),
						rs.getString("user_lastname"), rs.getString("user_email"), rs.getString("user_password"));

				Category categoryId = new Category(rs.getInt("category_id"), rs.getString("category_name"));

				Address addressId = new Address(rs.getInt("address_id"), rs.getString("address_name"));

				int jobMoney = rs.getInt("job_money");

				String description = rs.getString("job_description");

				Timestamp jobAppicationExpire = rs.getTimestamp("job_application_expire");

				int jobAppliedNumber = rs.getInt("job_applied_number");

				Timestamp jobCreate = rs.getTimestamp("job_created");

				Job j = new Job(jobId, jobName, user, categoryId, addressId, jobMoney, description, jobAppicationExpire,
						jobAppliedNumber, jobCreate);
				items.add(j);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		return items;
	}

}
