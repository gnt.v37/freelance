package models.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DJobDatabase {

	public static Connection Connect;
	
	public DJobDatabase ()
	{
		if (DJobDatabase.Connect == null) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				
				DJobDatabase.Connect = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=JobDB; IntegratedSecurity=True");
				
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			}
		}
	}
	
}
/**
 * USE [JobDB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[applyJob] (
@userID bigint,
@jobID bigint,
@money real,
@finishedIn nvarchar(100),
@description ntext,
@convince ntext,
@facebook ntext, 
@phone int, 
@email ntext
)
AS BEGIN 
	DECLARE @recrID bigint
	DECLARE @appliedUserName nvarchar(100)
	DECLARE @jobName nvarchar(256)
	DECLARE @emptyPhone int

	SELECT @recrID = jo.user_id FROM tbl_Jobs jo
	WHERE jo.job_id = @jobID

	SELECT @jobName = job_name FROM tbl_Jobs
	WHERE job_id = @jobID

	SELECT @appliedUserName = us.user_lastname FROM tbl_Users us
	WHERE us.user_id = @userID

	SELECT @emptyPhone = us.user_phone FROM tbl_Users us
	WHERE us.user_id = @userID

	IF @emptyPhone IS NULL
		UPDATE tbl_Users
		SET user_phone = @phone
		WHERE user_id = @userID
	

	INSERT INTO [dbo].[tbl_UserJobs]
           ([user_id]
           ,[job_id]
           ,[uj_money]
           ,[uj_finished_in]
           ,[uj_description]
           ,[uj_convince]
           ,[uj_facebook]
           ,[uj_phone]
           ,[uj_email]
           ,[uj_is_accept]
           ,[uj_apply_time])
     VALUES
           (@userID,
           @jobID,
           @money,
            @finishedIn,
           @description,
          @convince,
           @facebook,
           @phone,
           @email,
           0,
           GETDATE()
		   )


	INSERT INTO tbl_Notification(job_id, user_id, is_read, notification, notification_time)
	VALUES (@jobID, @recrID, 0, @appliedUserName + N' đã nạp đơn xin vào công việc ' + @jobName, GETDATE())

END
 */
