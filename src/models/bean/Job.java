package models.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class Job {
	
	private int id;
	private Category category;
	private User user;
	private Address address;
	private ArrayList<Ability> jobRequirement;
	private String name, description;
	private Double money;
	private Timestamp applicationExpire;
	private Timestamp created;
	private int appliedNumber;
	private int isPublish;
	
	public Job(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public Job (int id, User user, String name, Double money) {
		super();
		this.id = id;
		this.user = user;
		this.name = name;
		this.money = money;
		
	}
	
	public Job(int id, Category category, User user, Address address, ArrayList<Ability> jobRequirement, String name,
			String description, Double money, Timestamp applicationExpire, Timestamp created, int appliedNumber) {
		super();
		this.id = id;
		this.category = category;
		this.user = user;
		this.address = address;
		this.jobRequirement = jobRequirement;
		this.name = name;
		this.description = description;
		this.money = money;
		this.applicationExpire = applicationExpire;
		this.created = created;
		this.appliedNumber = appliedNumber;
	}
	
	public Job(int id, Category category, User user, Address address, ArrayList<Ability> jobRequirement, String name,
			String description, Double money, Timestamp applicationExpire, Timestamp created, int appliedNumber, int isPublish) {
		super();
		this.id = id;
		this.category = category;
		this.user = user;
		this.address = address;
		this.jobRequirement = jobRequirement;
		this.name = name;
		this.description = description;
		this.money = money;
		this.applicationExpire = applicationExpire;
		this.created = created;
		this.appliedNumber = appliedNumber;
		this.isPublish = isPublish;
	}
	
	public Job(int id, String name, User user, Category category, Address address, double money,
			String description, Timestamp applicationExpire, int appliedNumber, Timestamp created) {
		super();
		this.id = id;
		this.name = name;
		this.user = user;
		this.category = category;
		this.address = address;
		this.money = money;
		this.description = description;
		this.applicationExpire = applicationExpire;
		this.appliedNumber = appliedNumber;
		this.created = created;
	}

	
	public int getIsPublish() {
		return isPublish;
	}

	public void setIsPublish(int isPublish) {
		this.isPublish = isPublish;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public ArrayList<Ability> getJobRequirement() {
		return jobRequirement;
	}
	public void setJobRequirement(ArrayList<Ability> jobRequirement) {
		this.jobRequirement = jobRequirement;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
	public Date getApplicationExpire() {
		return applicationExpire;
	}
	public void setApplicationExpire(Timestamp applicationExpire) {
		this.applicationExpire = applicationExpire;
	}
	public int getAppliedNumber() {
		return appliedNumber;
	}
	public void setAppliedNumber(int appliedNumber) {
		this.appliedNumber = appliedNumber;
	}
	@Override
	public String toString() {
		return "Job [id=" + id + ", category=" + category + ", user=" + user + ", address=" + address
				+ ", jobRequirement=" + jobRequirement + ", name=" + name + ", description=" + description + ", money="
				+ money + ", applicationExpire=" + applicationExpire + ", appliedNumber=" + appliedNumber + "]";
	}
}
