package models.bean;

import java.util.Date;

public class Notification {

	private int id, jobID, userID, isRead;
	
	private String notification;

	private Date notificationTime;
	
	public Notification(int id, int jobID, int userID, int isRead, String notification, Date notificationTime) {
		super();
		this.id = id;
		this.jobID = jobID;
		this.userID = userID;
		this.isRead = isRead;
		this.notification = notification;
		this.notificationTime = notificationTime;
	}
	
	public Date getNotificationTime() {
		return notificationTime;
	}

	public void setNotificationTime(Date notificationTime) {
		this.notificationTime = notificationTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getJobID() {
		return jobID;
	}

	public void setJobID(int jobID) {
		this.jobID = jobID;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public int getIsRead() {
		return isRead;
	}

	public void setIsRead(int isRead) {
		this.isRead = isRead;
	}

	public String getNotification() {
		return notification;
	}

	public void setNotification(String notification) {
		this.notification = notification;
	}
	
}
