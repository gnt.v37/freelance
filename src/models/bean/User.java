package models.bean;

import java.util.ArrayList;
import java.util.Date;

public class User {

	private int id, jobApplied, jobRecruit, isAdmin, gender;
	private String firstName, lastName, title, description, phone, email, password, avatar;
	private Address address;
	private ArrayList<Ability> abilities;
	private ArrayList<Job> jobs;
	private Date created;

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public int getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(int isAdmin) {
		this.isAdmin = isAdmin;
	}

	public User(int id, String firstName, String lastName, String avatar) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.avatar = avatar;
	}

	public User(int id, String firstName, String lastName, String avatar, int isAdmin) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.avatar = avatar;
		this.isAdmin = isAdmin;
	}

	public User(int id, String firstName, String lastName, ArrayList<Ability> abilities, String avatar) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.avatar = avatar;
		this.abilities = abilities;
	}

	public User(int id, String firstName, String lastName, Address address) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
	}

	public User(int id, String firstName, String lastName, Address address, String description, String phone,
			String email, String password) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.description = description;
		this.phone = phone;
		this.email = email;
		this.password = password;
	}

	public User(int id, int jobApplied, int jobRecruit, String firstName, String lastName, Address address,
			String phone, String email, Date created, String avatar) {
		super();
		this.id = id;
		this.jobApplied = jobApplied;
		this.jobRecruit = jobRecruit;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.phone = phone;
		this.email = email;
		this.avatar = avatar;
		this.created = created;
	}

	public User(int id, int jobApplied, int jobRecruit, String firstName, String lastName, Address address,
			String description, String phone, String title, String email, String password, String avatar,
			ArrayList<Ability> abilities, ArrayList<Job> jobs, Date created) {
		super();
		this.id = id;
		this.jobApplied = jobApplied;
		this.jobRecruit = jobRecruit;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.description = description;
		this.phone = phone;
		this.title = title;
		this.email = email;
		this.password = password;
		this.avatar = avatar;
		this.created = created;
		this.abilities = abilities;
		this.jobs = jobs;
	}

	public User(int id, String firstName, String lastName, String email, String password) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
	}

	public User(int id, String firstName, String lastName, String description, String phone, String email,
			String password, String title, int jobApplied, int jobRecruit, Date created, String avatar,
			Address address) {
		// TODO Auto-generated constructor stub
		super();
		this.id = id;
		this.jobApplied = jobApplied;
		this.jobRecruit = jobRecruit;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.description = description;
		this.phone = phone;
		this.title = title;
		this.email = email;
		this.password = password;
		this.avatar = avatar;
		this.created = created;
	}

	public User(String firstName, String lastName, String phone, String email, String password, Date created,
			String avatar, Address address) {
		// TODO Auto-generated constructor stub
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.phone = phone;
		this.email = email;
		this.password = password;
		this.avatar = avatar;
		this.created = created;
	}

	public ArrayList<Job> getJobs() {
		return jobs;
	}

	public void setJobs(ArrayList<Job> jobs) {
		this.jobs = jobs;
	}

	public ArrayList<Ability> getAbilities() {
		return abilities;
	}

	public void setAbilities(ArrayList<Ability> abilities) {
		this.abilities = abilities;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getJobApplied() {
		return jobApplied;
	}

	public void setJobApplied(int jobApplied) {
		this.jobApplied = jobApplied;
	}

	public int getJobRecruit() {
		return jobRecruit;
	}

	public void setJobRecruit(int jobRecruit) {
		this.jobRecruit = jobRecruit;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", jobApplied=" + jobApplied + ", jobRecruit=" + jobRecruit + ", firstName="
				+ firstName + ", lastName=" + lastName + ", address=" + address + ", title=" + title + ", description="
				+ description + ", phone=" + phone + ", email=" + email + ", password=" + password + ", created="
				+ created + "]";
	}

}
