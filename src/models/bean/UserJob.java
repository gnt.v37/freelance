package models.bean;

import java.sql.Timestamp;
import java.util.Date;

public class UserJob {
	
	private User user;
	
	private Job job;
	
	private double money;
	
	Timestamp applyTime, acceptTime;
	
	private String finishedIn, description, convince, facebook, phone, email, isApplied;
	
	public UserJob(User user, double money, String finishedIn, String description, String convince,
			String facebook, String phone, String email, String isApplied, Timestamp applyTime, Timestamp acceptTime) {
		super();
		this.user = user;
		this.money = money;
		this.finishedIn = finishedIn;
		this.description = description;
		this.convince = convince;
		this.facebook = facebook;
		this.phone = phone;
		this.email = email;
		this.isApplied = isApplied;
		this.applyTime = applyTime;
		this.acceptTime = acceptTime;
	} 
	
	public UserJob(Job job, double money, String isApplied, Timestamp applyTime, Timestamp acceptTime, String finishedIn,
			String description, String convince, String facebook, String phone, String email) {
		// TODO Auto-generated constructor stub
		super();
		this.job = job;
		this.money = money;
		this.finishedIn = finishedIn;
		this.description = description;
		this.convince = convince;
		this.facebook = facebook;
		this.phone = phone;
		this.email = email;
		this.isApplied = isApplied;
		this.applyTime = applyTime;
		this.acceptTime = acceptTime;
	}

	public Date getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(Timestamp applyTime) {
		this.applyTime = applyTime;
	}

	public Date getAcceptTime() {
		return acceptTime;
	}

	public void setAcceptTime(Timestamp acceptTime) {
		this.acceptTime = acceptTime;
	}

	public String getIsApplied() {
		return isApplied;
	}

	public void setIsApplied(String isApplied) {
		this.isApplied = isApplied;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public String getFinishedIn() {
		return finishedIn;
	}

	public void setFinishedIn(String finishedIn) {
		this.finishedIn = finishedIn;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConvince() {
		return convince;
	}

	public void setConvince(String convince) {
		this.convince = convince;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	 
	

}
